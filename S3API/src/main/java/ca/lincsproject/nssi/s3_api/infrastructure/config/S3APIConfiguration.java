package ca.lincsproject.nssi.s3_api.infrastructure.config;

import ca.lincsproject.nssi.s3_api.S3APIModuleConstants;
import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;

@Profile(S3APIModuleConstants.PROFILE_NAME)
@Configuration
public class S3APIConfiguration {

    @Lazy
    @Bean
    public MinioClient lincsMinioClient(@Value("${nssi.s3.endpoint:https://aux.lincsproject.ca}") String endpoint,
                                        @Value("${nssi.s3.access_key}") String access,
                                        @Value("${nssi.s3.secret_key}") String secretKey,
                                        @Value("${nssi.s3.region:ca}") String region) {
        return MinioClient.builder()
                .endpoint(endpoint)
                .credentials(access, secretKey)
                .region(region)
                .build();
    }
}
