package ca.lincsproject.nssi.s3_api.repository;

import ca.lincsproject.nssi.s3_api.S3APIModuleConstants;
import io.minio.*;
import io.minio.messages.DeleteError;
import io.minio.messages.DeleteObject;
import io.minio.messages.Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Repository;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.StreamSupport;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(S3APIModuleConstants.PROFILE_NAME)
@Repository
public class LincsS3Repository {

    private final MinioClient minioClient;

    private final Logger LOGGER = LoggerFactory.getLogger(LincsS3Repository.class);

    @Lazy
    @Autowired
    LincsS3Repository(MinioClient minioClient) {
        this.minioClient = minioClient;
    }

    public byte[] getObject(String bucket, String object, String requestId) {
        try (InputStream inputStream = minioClient.getObject(
                GetObjectArgs.builder()
                        .bucket(bucket)
                        .object(object)
                        .build())
        ) {
            return inputStream.readAllBytes();
        } catch (Exception e) {
            LOGGER.error("FAILED To Get Object",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", S3APIModuleConstants.SERVICE_NAME),
                        kv("bucket", bucket),
                        kv("object", object),
                        kv("requestId", requestId));
        }

        return new byte[0];
    }

    public List<Pair<String, byte[]>> getAllObjects(String bucket, String objectPrefix, String requestId) {
        try {
            Iterable<Result<Item>> objects = () ->
                    StreamSupport.stream(listObjects(bucket, objectPrefix).spliterator(), false)
                            .iterator();

            List<Pair<String, byte[]>> result = new ArrayList<>();

            for (Result<Item> objectResult : objects) {
                InputStream object = minioClient.getObject(
                        GetObjectArgs.builder()
                                .bucket(bucket)
                                .object(objectResult.get().objectName())
                                .build()
                );

                result.add(Pair.of(objectResult.get().objectName(), object.readAllBytes()));
            }

            return result;
        } catch (Exception e) {
            LOGGER.error("FAILED To Get Object(s)",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", S3APIModuleConstants.SERVICE_NAME),
                        kv("bucket", bucket),
                        kv("objectPrefix", objectPrefix),
                        kv("requestId", requestId));
        }

        return Collections.emptyList();
    }

    public void putObject(String bucket, String object, byte[] contents, String requestId) {
        try {
            minioClient.putObject(
                    PutObjectArgs.builder()
                            .bucket(bucket)
                            .object(object)
                            .stream(new ByteArrayInputStream(contents), contents.length, -1)
                            .build()
            );
        } catch (Exception e) {
            LOGGER.error("FAILED To Put Object",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", S3APIModuleConstants.SERVICE_NAME),
                        kv("bucket", bucket),
                        kv("object", object),
                        kv("requestId", requestId));
        }
    }

    public void deleteObject(String bucket, String object, String requestId) {
        try {
            minioClient.removeObject(
                    RemoveObjectArgs.builder()
                            .bucket(bucket)
                            .object(object)
                            .build()
            );
        } catch (Exception e) {
            LOGGER.error("FAILED To Delete Object",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", S3APIModuleConstants.SERVICE_NAME),
                        kv("bucket", bucket),
                        kv("object", object),
                        kv("requestId", requestId));
        }
    }

    public void deleteAllObjects(String bucket, String objectPrefix, String requestId) {
        LOGGER.info("Deleting Multiple Objects",  
                   kv("service", S3APIModuleConstants.SERVICE_NAME),
                   kv("bucket", bucket),
                   kv("objectPrefix", objectPrefix),
                   kv("requestId", requestId));
        var objects = listObjects(bucket, objectPrefix);
        List<DeleteObject> deleteObjects = new ArrayList<>();
        for (Result<Item> objectResult : objects) {
            try {
                deleteObjects.add(new DeleteObject(objectResult.get().objectName()));
            } catch (Exception e) {
                LOGGER.error("FAILED To Delete Object(s)",  
                            kv("stackTrace", e.getStackTrace()), 
                            kv("service", S3APIModuleConstants.SERVICE_NAME),
                            kv("bucket", bucket),
                            kv("objectPrefix", objectPrefix),
                            kv("requestId", requestId));
            }
        }

        var deleteResults = minioClient.removeObjects(
                RemoveObjectsArgs.builder()
                        .bucket(bucket)
                        .objects(deleteObjects)
                        .build()
        );

        for (Result<DeleteError> deleteResult : deleteResults) {
            try {
                DeleteError error = deleteResult.get();
                LOGGER.error("FAILED To Delete Object",
                            kv("stackTrace", error.message()),  
                            kv("service", S3APIModuleConstants.SERVICE_NAME),
                            kv("bucket", error.bucketName()),
                            kv("object", error.objectName()),
                            kv("requestId", requestId));
            } catch (Exception e) {
                LOGGER.error("FAILED To Get Delete Error",
                            kv("stackTrace", e.getStackTrace()),  
                            kv("service", S3APIModuleConstants.SERVICE_NAME),
                            kv("bucket", bucket),
                            kv("objectPrefix", objectPrefix),
                            kv("requestId", requestId));
            }
        }
    }

    public List<String> getObjectNamesFromDir(String bucket, String objectPrefix, String requestId) {
        ArrayList<String> files = new ArrayList<>();
        Iterable<Result<Item>> objects = listObjects(bucket, objectPrefix);
        for (Result<Item> objectResult : objects) {
            try {
                files.add(objectResult.get().objectName());
            } catch (Exception e) {
                LOGGER.error("FAILED To Get Object Names From Directory",
                            kv("stackTrace", e.getStackTrace()),  
                            kv("service", S3APIModuleConstants.SERVICE_NAME),
                            kv("bucket", bucket),
                            kv("objectPrefix", objectPrefix),
                            kv("requestId", requestId));
            }
        }
        return files;
    }

    private Iterable<Result<Item>> listObjects(String bucket, String objectPrefix) {
        return minioClient.listObjects(
                ListObjectsArgs.builder()
                        .bucket(bucket)
                        .prefix(objectPrefix)
                        .recursive(true)
                        .build());
    }
}
