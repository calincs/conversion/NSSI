package ca.lincsproject.nssi.stanfordner.service;

import ca.lincsproject.nssi.broker.service.RMQUtilsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("stanford")
@SpringBootTest
public class XMLProcessorTest {

    @MockBean
    private RMQUtilsService rmqUtils;

    @Test
    public void resultMessage() 
    {
        XMLProcessorUtils processor = new XMLProcessorUtils(new ObjectMapper());

        Assert.assertTrue(processor.cleanLemma("testtest12").equals("testtest12"));
        Assert.assertTrue(processor.cleanLemma("\"test\"   1").equals("test 1"));
        Assert.assertTrue(processor.cleanLemma("\"test     123\"\"\"").equals("test 123"));
    }

}