package ca.lincsproject.nssi.linking;

import org.springframework.boot.autoconfigure.SpringBootApplication;

// Needed for initializing tests.

@SpringBootApplication
public class TestApplication {
}
