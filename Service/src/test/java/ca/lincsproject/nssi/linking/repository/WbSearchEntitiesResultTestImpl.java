package ca.lincsproject.nssi.linking.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.wikidata.wdtk.wikibaseapi.WbSearchEntitiesResult;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WbSearchEntitiesResultTestImpl implements WbSearchEntitiesResult {

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MatchImpl implements Match {
        private String type;
        private String language;
        private String text;
    }

    private String entityId;
    private String conceptUri;
    private String url;
    private String title;
    private long pageId;
    private String label;
    private String description;
    private MatchImpl match;
    private List<String> aliases;
}
