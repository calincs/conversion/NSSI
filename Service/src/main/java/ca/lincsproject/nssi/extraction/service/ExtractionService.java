package ca.lincsproject.nssi.extraction.service;

import ca.lincsproject.nssi.broker.service.NSSIServiceUtilsFactory;
import ca.lincsproject.nssi.broker.message.NSSIServiceMessage;
import ca.lincsproject.nssi.broker.service.NSSIServiceUtils;
import ca.lincsproject.nssi.extraction.ExtractionModuleConstants;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Optional;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(ExtractionModuleConstants.PROFILE_NAME)
@Service
public class ExtractionService {
    private final NSSIServiceUtils<Void, String> nssiServiceUtils;
    private final RestTemplate restTemplate;

    private final Logger LOGGER = LoggerFactory.getLogger(ExtractionService.class);

    @Autowired
    public ExtractionService(@Value("${nssi.processing.services.extraction.fullName}") String fullName,
                             NSSIServiceUtilsFactory nssiServiceUtilsFactory,
                             RestTemplate restTemplate)
    {
        this.nssiServiceUtils = nssiServiceUtilsFactory.nssiServiceUtils(
                Void.class,
                String.class,
                fullName
        );
        this.restTemplate = restTemplate;
    }

    @RabbitListener(queues = "extractionQueue")
    public void listen(Message message) {
        nssiServiceUtils.processWithRetry(message, this::processMessage);
    }

    private void processMessage(NSSIServiceMessage message, Void unused) throws Throwable {
        LOGGER.info("Processing Extraction Message",
                kv("service", ExtractionModuleConstants.SERVICE_NAME),
                kv("jobId", message.getJobId()),
                kv("documentUri", message.getDocumentURI()),
                kv("requestId", message.getRequestId()));

        documentContents(message)
                .ifPresent(contents -> nssiServiceUtils.sendToNextStep(message, contents));
    }

    private Optional<String> documentContents(NSSIServiceMessage message) {
        String documentURI = message.getDocumentURI();

        switch (message.getFormat()) {
            case TEI_XML:
                return fetchTEIXML(documentURI);
            case ORLANDO_XML:
                return fetchOrlandoXML(documentURI);
            case PDF:
                return fetchPDF(documentURI);
            case HTML:
                return fetchHTML(documentURI);
            case PLAIN_TEXT:
                return fetchText(documentURI);
            default:
                return Optional.empty();
        }
    }

    private Optional<String> fetchTEIXML(String documentURI) {
        String result = restTemplate.getForObject(documentURI, String.class);

        return Optional.ofNullable(result);
    }

    private Optional<String> fetchOrlandoXML(String documentURI) {
        String result = restTemplate.getForObject(documentURI, String.class);

        return Optional.ofNullable(result);
    }

    private Optional<String> fetchPDF(String documentURI) {
        // TODO
        return Optional.empty();
    }

    private Optional<String> fetchHTML(String documentURI) {
        try {
            Document document = Jsoup.connect(documentURI).get();
            Element body = document.select("body").first(); // TODO??

            return Optional.ofNullable(body).map(Element::text);
        } catch (HttpStatusException e) {
            // TODO
            return Optional.empty();
        } catch (IOException e) {
            // TODO
            return Optional.empty();
        }
    }

    private Optional<String> fetchText(String documentURI) {
        String result = restTemplate.getForObject(documentURI, String.class);

        return Optional.ofNullable(result);
    }
}
