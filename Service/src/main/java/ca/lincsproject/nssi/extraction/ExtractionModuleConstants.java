package ca.lincsproject.nssi.extraction;

public class ExtractionModuleConstants {
    public static final String PROFILE_NAME = "extraction";
    public static final String QUEUE_NAME = "extractionQueue";
    public static final String SERVICE_NAME = "Extraction Service";
}
