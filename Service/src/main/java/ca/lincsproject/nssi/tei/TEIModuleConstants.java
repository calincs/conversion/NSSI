package ca.lincsproject.nssi.tei;

public class TEIModuleConstants {
    public static final String PROFILE_NAME = "tei";
    public static final String QUEUE_NAME = "teiQueue";
    public static final String SERVICE_NAME = "TEI -ography Service";
}
