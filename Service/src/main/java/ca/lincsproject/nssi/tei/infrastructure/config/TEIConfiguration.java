package ca.lincsproject.nssi.tei.infrastructure.config;

import ca.lincsproject.nssi.broker.infrastructure.config.RMQConfiguration;
import ca.lincsproject.nssi.tei.TEIModuleConstants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile(TEIModuleConstants.PROFILE_NAME)
@Configuration
public class TEIConfiguration {
    @Bean
    public Queue teiQueue() {
        return QueueBuilder.durable(TEIModuleConstants.QUEUE_NAME)
                .withArgument(RMQConfiguration.DLQ_KEY, RMQConfiguration.DLX_APP_MESSAGES)
                .build();
    }

    @Bean
    public Binding teiBinding(@Qualifier(RMQConfiguration.APP_MESSAGES_EXCHANGE) DirectExchange exchange) {
        return BindingBuilder.bind(teiQueue()).to(exchange)
                .with(TEIModuleConstants.QUEUE_NAME);
    }
}
