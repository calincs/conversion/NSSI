package ca.lincsproject.nssi.s3sink.service;

import ca.lincsproject.nssi.broker.message.NSSIServiceMessage;
import ca.lincsproject.nssi.broker.service.NSSIServiceUtils;
import ca.lincsproject.nssi.broker.service.NSSIServiceUtilsFactory;
import ca.lincsproject.nssi.s3sink.S3SinkModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile(S3SinkModuleConstants.PROFILE_NAME)
@Service
public class S3SinkListenerService {
    private final S3SinkService s3SinkService;
    private final NSSIServiceUtils<String, Void> nssiServiceUtils;

    private final Logger LOGGER = LoggerFactory.getLogger(S3SinkListenerService.class);

    @Autowired
    S3SinkListenerService(S3SinkService s3SinkService,
                          @Value("${nssi.processing.services.s3sink.fullName}") String fullName,
                          NSSIServiceUtilsFactory nssiServiceUtilsFactory)
    {
        this.s3SinkService = s3SinkService;
        this.nssiServiceUtils = nssiServiceUtilsFactory.nssiServiceUtils(
                String.class,
                Void.class,
                fullName
        );
    }

    @RabbitListener(queues = "s3sinkQueue")
    public void listen(Message message) {
        nssiServiceUtils.processWithRetry(message, this::processMessage);
    }

    private void processMessage(NSSIServiceMessage message, String document) throws Throwable {
        s3SinkService.saveDocument(message.getJobId(), message.getRequestId(), message.getMessageId(), document);
    }
}
