package ca.lincsproject.nssi.s3sink;

public class S3SinkModuleConstants {
    public static final String PROFILE_NAME = "s3sink";
    public static final String QUEUE_NAME = "s3sinkQueue";
    public static final String SERVICE_NAME = "S3 Sink Service";
    public static final String BUCKET_NAME = "lincs-apps"; // should this be a config property?
    public static final String OBJECT_PREFIX_FORMAT = "nssi/%s/";
    public static final String OBJECT_NAME_FORMAT = "nssi/%s/%s";
}
