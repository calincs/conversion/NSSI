package ca.lincsproject.nssi.s3sink.service;

import ca.lincsproject.nssi.broker.message.NSSIServiceMessage;
import ca.lincsproject.nssi.broker.message.ResultMessage;
import ca.lincsproject.nssi.broker.service.RMQUtilsService;
import ca.lincsproject.nssi.s3_api.service.LincsS3Service;
import ca.lincsproject.nssi.s3sink.S3SinkModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.UUID;

@Profile(S3SinkModuleConstants.PROFILE_NAME)
@Service
public class S3SinkService {
    private final LincsS3Service lincsS3Service;
    private final RMQUtilsService rmqUtils;

    private final Logger LOGGER = LoggerFactory.getLogger(S3SinkService.class);

    @Autowired
    S3SinkService(LincsS3Service lincsS3Service,
                  RMQUtilsService rmqUtils)
    {
        this.lincsS3Service = lincsS3Service;
        this.rmqUtils = rmqUtils;
    }

    public void saveDocument(Long jobId, String requestId, String messageId, String document) {
        lincsS3Service.putObject(
                S3SinkModuleConstants.BUCKET_NAME,
                String.format(S3SinkModuleConstants.OBJECT_NAME_FORMAT, jobId, UUID.randomUUID()),
                document.getBytes(StandardCharsets.UTF_8),
                requestId
        );

        ResultMessage resultMessage = ResultMessage.builder()
                .jobId(jobId)
                .requestId(requestId)
                .resultStoreParams(Map.of(
                        "S3_BUCKET_NAME", S3SinkModuleConstants.BUCKET_NAME,
                        "S3_OBJECT_PREFIX", String.format(S3SinkModuleConstants.OBJECT_PREFIX_FORMAT, jobId)
                ))
                .build();

        rmqUtils.sendResultMessage(resultMessage);

        LOGGER.info("S3SINK Sending progress message from S3Sink");
        NSSIServiceMessage message = NSSIServiceMessage.builder()
                .jobId(jobId)
                .requestId(requestId)
                .messageId(messageId)
                .build();

        rmqUtils.sendToNextStep(S3SinkService.class, message);
    }
}
