package ca.lincsproject.nssi.dbsink.service;

import ca.lincsproject.nssi.job.repository.Job;
import ca.lincsproject.nssi.job.repository.JobRepository;
import ca.lincsproject.nssi.job.repository.Result;
import ca.lincsproject.nssi.job.repository.ResultRepository;
import ca.lincsproject.nssi.broker.service.RMQUtilsService;
import ca.lincsproject.nssi.dbsink.DBSinkModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Profile(DBSinkModuleConstants.PROFILE_NAME)
@Service
public class DBSinkService {
    private final ResultRepository resultRepository;
    private final JobRepository jobRepository;
    private final RMQUtilsService rmqUtils;

    private final Logger LOGGER = LoggerFactory.getLogger(DBSinkService.class);

    @Autowired
    DBSinkService(ResultRepository resultRepository,
                  JobRepository jobRepository,
                  RMQUtilsService rmqUtils)
    {
        this.resultRepository = resultRepository;
        this.jobRepository = jobRepository;
        this.rmqUtils = rmqUtils;
    }

    public void saveDocument(Long jobId, String requestId, String messageId, String document) {
        Optional<Job> job = jobRepository.findById(jobId);

        if (job.isEmpty()) {
            return;
        }

        Result result = Result.builder()
                .job(job.get())
                .isOnS3(false)
                .document(document)
                .build();

        resultRepository.save(result);
    }
}
