package ca.lincsproject.nssi.dbsink.service;

import ca.lincsproject.nssi.broker.service.NSSIServiceUtilsFactory;
import ca.lincsproject.nssi.broker.message.NSSIServiceMessage;
import ca.lincsproject.nssi.broker.service.NSSIServiceUtils;
import ca.lincsproject.nssi.dbsink.DBSinkModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile(DBSinkModuleConstants.PROFILE_NAME)
@Service
public class DBSinkListenerService {
    private final DBSinkService dbSinkService;
    private final NSSIServiceUtils<String, Void> nssiServiceUtils;

    private final Logger LOGGER = LoggerFactory.getLogger(DBSinkListenerService.class);

    @Autowired
    DBSinkListenerService(DBSinkService dbSinkService,
                          @Value("${nssi.processing.services.dbsink.fullName}") String fullName,
                          NSSIServiceUtilsFactory nssiServiceUtilsFactory)
    {
        this.dbSinkService = dbSinkService;
        this.nssiServiceUtils = nssiServiceUtilsFactory.nssiServiceUtils(
                String.class,
                Void.class,
                fullName
        );
    }

    @RabbitListener(queues = "dbsinkQueue")
    public void listen(Message message) {
        nssiServiceUtils.processWithRetry(message, this::processMessage);
    }

    private void processMessage(NSSIServiceMessage message, String document) throws Throwable {
        dbSinkService.saveDocument(message.getJobId(), message.getRequestId(), message.getMessageId(), document);
    }
}
