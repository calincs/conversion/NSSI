package ca.lincsproject.nssi.dbsink.infrastructure.config;

import ca.lincsproject.nssi.dbsink.DBSinkModuleConstants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static ca.lincsproject.nssi.broker.infrastructure.config.RMQConfiguration.APP_MESSAGES_EXCHANGE;
import static ca.lincsproject.nssi.broker.infrastructure.config.RMQConfiguration.DLQ_KEY;
import static ca.lincsproject.nssi.broker.infrastructure.config.RMQConfiguration.DLX_APP_MESSAGES;

@Profile(DBSinkModuleConstants.PROFILE_NAME)
@Configuration
public class DBSinkConfiguration {
    @Bean
    public Queue dbSinkQueue(@Value("${nssi.processing.services.dbsink.jobQueue}") String queueName) {
        return QueueBuilder.durable(queueName)
                .withArgument(DLQ_KEY, DLX_APP_MESSAGES)
                .build();
    }

    @Bean
    public Binding dbSinkBinding(@Qualifier(APP_MESSAGES_EXCHANGE) DirectExchange exchange,
                                  @Value("${nssi.processing.services.dbsink.jobQueue}") String queueName) {
        return BindingBuilder.bind(dbSinkQueue(queueName)).to(exchange)
                .with(queueName);
    }
}
