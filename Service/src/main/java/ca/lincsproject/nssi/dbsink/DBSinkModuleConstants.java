package ca.lincsproject.nssi.dbsink;

public class DBSinkModuleConstants {
    public static final String SERVICE_NAME = "Database Sink Service";
    public static final String PROFILE_NAME = "dbsink";
}
