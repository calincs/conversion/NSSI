package ca.lincsproject.nssi.api.infrastructure.config;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Configuration
@Validated
@ConfigurationProperties(prefix = "nssi.processing")
public class NSSIProcessingConfigProperties implements Validator {

    private Map<String, ServiceConfig> services;
    private Map<String, WorkflowConfig> workflows;

    @Override
    public boolean supports(Class<?> type) {
        return NSSIProcessingConfigProperties.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object target, Errors errors) {
        NSSIProcessingConfigProperties properties = (NSSIProcessingConfigProperties) target;
        var serviceConfigs = properties.getServices();
        var workflowConfigs = properties.getWorkflows();

        workflowConfigs.forEach((workflow, config) -> {
            var pipelineSteps = config.pipeline.split(",");
            Arrays.stream(pipelineSteps)
                    .filter(serviceName -> !serviceConfigs.containsKey(serviceName))
                    .findFirst()
                    .ifPresent(serviceName ->
                            errors.rejectValue(workflow,
                                    "service.not.defined",
                                    new Object[]{serviceName},
                                    "The workflow pipeline contains a service, '"
                                            + serviceName + "', that has not been defined.")
                    );
        });
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ServiceConfig {
        private String fullName;

        @NotEmpty
        private String jobQueue;

        @NotEmpty
        private String uri;

        private String cleanupQueue;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class WorkflowConfig {
        private String fullName;

        @NotEmpty
        @Getter(AccessLevel.NONE)
        private String pipeline;

        @NotNull
        private WorkflowResultsType resultsType;
    }

    public List<ServiceConfig> getServiceConfigs(String workflow) {
        return Arrays.stream(workflows.get(workflow).pipeline.split(","))
                .map(services::get)
                .collect(Collectors.toList());
    }
}
