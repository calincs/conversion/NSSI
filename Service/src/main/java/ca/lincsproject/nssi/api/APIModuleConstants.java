package ca.lincsproject.nssi.api;

public class APIModuleConstants {
    public static final String PROFILE_NAME = "api";
    public static final String SERVICE_NAME = "API Service";
}
