package ca.lincsproject.nssi.api.infrastructure.config;

import ca.lincsproject.nssi.api.APIModuleConstants;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.authorization.client.AuthzClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Map;

@Profile(APIModuleConstants.PROFILE_NAME)
@Configuration
public class APIConfiguration {

    private static final String ADMIN_CLIENT = "admin-cli";

    @Bean
    public Keycloak keycloakAdminClient(@Value("${keycloak.auth-server-url}") String authServerUrl,
                                        @Value("${keycloak.realm}") String authRealm,
                                        @Value("${nssi.keycloak.admin-client.secret}") String adminClientSecret)
    {
        return KeycloakBuilder.builder()
                .serverUrl(authServerUrl)
                .realm(authRealm)
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                .clientId(ADMIN_CLIENT)
                .clientSecret(adminClientSecret)
                .build();
    }


    @Bean
    public AuthzClient authzClient(@Value("${keycloak.realm}") String authRealm,
                                   @Value("${keycloak.resource}") String client,
                                   @Value("${keycloak.credentials.secret}") String secret,
                                   @Value("${keycloak.auth-server-url}") String authServerUrl)
    {
        Map<String, Object> credentials = Map.of("secret", secret);
        org.keycloak.authorization.client.Configuration authClientConfig =
                new org.keycloak.authorization.client.Configuration(
                        authServerUrl,
                        authRealm,
                        client,
                        credentials,
                        null
                );
        return AuthzClient.create(authClientConfig);
    }
}
