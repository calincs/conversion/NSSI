package ca.lincsproject.nssi.api.web;

import ca.lincsproject.nssi.api.infrastructure.config.NSSIProcessingConfigProperties;
import ca.lincsproject.nssi.api.APIModuleConstants;
import org.keycloak.representations.idm.authorization.Permission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * This controller provides access control information about resources involved
 * with data processing tasks, such as the authorities that a user may select
 * for entity linking, or the workflows they are authorized to use.
 */

@Profile(APIModuleConstants.PROFILE_NAME)
@RestController
public class AuthorizationsController {
    private final NSSIProcessingConfigProperties processingConfigProperties;
    private final String client;
    private final RestTemplate restTemplate;

    private final String UMA_TICKET_GRANT_TYPE = "urn:ietf:params:oauth:grant-type:uma-ticket";
    private final String WORKFLOW_SCOPE = "urn:nssi:workflow";
    private final String keycloak_auth_url;
    private final String realm;

    private final Logger LOGGER = LoggerFactory.getLogger(AuthorizationsController.class);

    @Autowired
    AuthorizationsController(NSSIProcessingConfigProperties processingConfigProperties,
                             @Value("${keycloak.resource}") String client,
                             @Value("${keycloak.auth-server-url}") String keycloak_auth_url,
                             @Value("${keycloak.realm}") String realm,
                             RestTemplate restTemplate)
    {
        this.processingConfigProperties = processingConfigProperties;
        this.client = client;
        this.keycloak_auth_url = keycloak_auth_url;
        this.realm = realm;
        this.restTemplate = restTemplate;
    }

    @GetMapping("/api/authorities")
    public ResponseEntity<List<String>> getAllowedAuthorities(Principal principal,
                                                              @RequestHeader("Authorization") String authToken,
                                                              @RequestHeader("requestId") String requestId)
    {
        // TODO
        return ResponseEntity.ok(Collections.singletonList("TODO"));
    }

    /**
     * Returns a list of workflows for which the authenticated user has access.
     *
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    @GetMapping("/api/workflows")
    public ResponseEntity<Map<String, WorkflowInfo>> getAllowedWorkflows(Principal principal,
                                                                         @RequestHeader("Authorization") String authToken,
                                                                         @RequestHeader("requestId") String requestId)
    {
        List<String> allowedWorkflowsForUser = checkAllowed(authToken.substring(7));
        Map<String, WorkflowInfo> authorizedWorkflows = allowedWorkflowsForUser.stream()
                .collect(Collectors.toMap(
                        workflowName -> "/api/workflows/" + workflowName,
                        workflowName -> WorkflowInfo.builder()
                                .shortName(workflowName)
                                .fullName(processingConfigProperties.getWorkflows().get(workflowName).getFullName())
                                .pipeline(processingConfigProperties.getServiceConfigs(workflowName).stream()
                                        .map(NSSIProcessingConfigProperties.ServiceConfig::getFullName)
                                        .collect(Collectors.toList()))
                                .resultsTypeName(processingConfigProperties.getWorkflows().get(workflowName)
                                        .getResultsType().name())
                                .build()));

        return ResponseEntity.ok(authorizedWorkflows);
    }

    private List<String> checkAllowed(String authToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(authToken);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.add("grant_type", UMA_TICKET_GRANT_TYPE);
        formData.add("response_include_resource_name", "true");
        formData.add("response_mode", "permissions");
        formData.add("audience", client);

        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(formData, headers);

        try {
            ResponseEntity<List<Permission>> response = restTemplate.exchange(
                    keycloak_auth_url + "realms/" + realm + "/protocol/openid-connect/token",
                    HttpMethod.POST,
                    httpEntity,
                    new ParameterizedTypeReference<>() {
                    }
            );

            return Objects.requireNonNull(response.getBody()).stream()
                    .filter(permission -> permission.getScopes().contains(WORKFLOW_SCOPE))
                    .map(Permission::getResourceName)
                    .collect(Collectors.toList());

        } catch (HttpClientErrorException e) {
            LOGGER.error(e.getMessage());
            return Collections.emptyList();
        }
    }
}
