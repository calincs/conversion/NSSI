package ca.lincsproject.nssi.api.web;

import ca.lincsproject.nssi.job.model.JobStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JobInfo {
    /**
     * The job ID.
     */
    private Long jobId;

    /**
     * The status of the job.
     */
    private JobStatus status;

    /**
     * A URI from where job results can be obtained.
     */
    private String resultsUri;

    /**
     * An ETA based off the specified workflow.
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double estimatedMinUntilRun;
}
