package ca.lincsproject.nssi.api.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WorkflowInfo {
    /**
     * The short name of the workflow.
     */
    private String shortName;

    /**
     * The full name of the workflow.
     */
    private String fullName;

    /**
     * The list of services that make up the workflow's processing pipeline.
     */
    private List<String> pipeline;

    /**
     * The type of results produced by the workflow.
     */
    private String resultsTypeName;
}
