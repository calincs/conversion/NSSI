package ca.lincsproject.nssi.api.service;

import ca.lincsproject.nssi.apiexception.infrastructure.exception.ResourceNotFoundException;
import ca.lincsproject.nssi.api.APIModuleConstants;
import ca.lincsproject.nssi.job.model.JobDTO;
import ca.lincsproject.nssi.s3_api.service.LincsS3Service;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(APIModuleConstants.PROFILE_NAME)
@Service
public class S3ResultsService {
    private final LincsS3Service s3Service;
    private final ObjectMapper objectMapper;

    private final String BUCKET_NAME_KEY = "S3_BUCKET_NAME";
    private final String OBJECT_NAME_KEY = "S3_OBJECT_NAME";
    private final String OBJECT_PREFIX_KEY = "S3_OBJECT_PREFIX";
    private final String METADATA_OBJECT_NAME_KEY = "S3_METADATA_OBJECT";
    private final String PLAIN_TEXT_KEY = "S3_PLAIN_TEXT";

    private final TypeReference<HashMap<String, String>> STR_MAP_TYPEREF = new TypeReference<>() {
    };
    private final TypeReference<List<List<Map<String, Object>>>> OBJ_MAP_LIST_TYPEREF = new TypeReference<>() {
    };

    private final Logger LOGGER = LoggerFactory.getLogger(S3ResultsService.class);

    @Autowired
    S3ResultsService(LincsS3Service s3Service,
                     ObjectMapper objectMapper)
    {
        this.s3Service = s3Service;
        this.objectMapper = objectMapper;
    }

    public S3Result getResults(JobDTO job, String requestId) {
        if (job.getResults().isEmpty()) {
            throw new ResourceNotFoundException();
        }

        Map<String, String> params = job.getResults().get(0).getResultStoreParams();
        boolean plainText = Boolean.parseBoolean(params.get(PLAIN_TEXT_KEY));
        
        if (plainText) {
            return TextResult.builder()
                    .processingDate(job.getLastUpdatedTime())
                    .metadata(getS3Metadata(job, requestId))
                    .data(getTextData(job, requestId))
                    .build();
        }

        return ReconciliationJsonResult.builder()
                .processingDate(job.getLastUpdatedTime())
                .metadata(getS3Metadata(job, requestId))
                .data(getJsonData(job, requestId))
                .build();
    }

    public void deleteResults(JobDTO job, String requestId)
    {
        if (job.getResults().isEmpty()) {
            throw new ResourceNotFoundException();
        }

        Map<String, String> params = job.getResults().get(0).getResultStoreParams();
        String bucket = params.get(BUCKET_NAME_KEY);
        String object = params.get(OBJECT_NAME_KEY);
        String objectPrefix = params.get(OBJECT_PREFIX_KEY);

        if (StringUtils.isNotEmpty(bucket)) {
            if (StringUtils.isNotEmpty(object)) {
                s3Service.deleteObject(bucket, object, requestId);
            } else if (StringUtils.isNotEmpty(objectPrefix)) {
                s3Service.deleteAllObjects(bucket, objectPrefix, requestId);
            }
        }
    }

    private Map<String, String> getS3Metadata(JobDTO job, String requestId) {
        if (job.getResults().isEmpty()) {
            throw new ResourceNotFoundException();
        }

        try {
            Map<String, String> params = job.getResults().get(0).getResultStoreParams();
            String bucket = params.get(BUCKET_NAME_KEY);
            String metadataObject = params.get(METADATA_OBJECT_NAME_KEY);

            if (StringUtils.isNotEmpty(bucket)) {
                if (StringUtils.isNotEmpty(metadataObject)) {
                    byte[] metadataContents = s3Service.getObject(bucket, metadataObject, requestId);
                    return objectMapper.readValue(metadataContents, STR_MAP_TYPEREF);
                }
            }

        } catch (IOException e) {
            LOGGER.error("FAILED To Get S3 Metadata",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", APIModuleConstants.SERVICE_NAME),
                        kv("jobId", job.getId()),
                        kv("requestId", requestId));
        }

        return Collections.emptyMap();
    }


    private List<List<Map<String, Object>>> getJsonData(JobDTO job, String requestId) {
        if (job.getResults().isEmpty()) {
            throw new ResourceNotFoundException();
        }

        List<List<Map<String,Object>>> allContents = new ArrayList<>();
        try {
            Map<String, String> params = job.getResults().get(0).getResultStoreParams();
            String bucket = params.get(BUCKET_NAME_KEY);
            String object = params.get(OBJECT_NAME_KEY);

            if (StringUtils.isNotEmpty(bucket)) {
                if (StringUtils.isNotEmpty(object)) {
                    byte[] resultsContents = s3Service.getObject(bucket, object, requestId);
                    return objectMapper.readValue(resultsContents, OBJ_MAP_LIST_TYPEREF);
                }
            }
        } catch (IOException e) {
            LOGGER.error("FAILED To Get Json Data",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", APIModuleConstants.SERVICE_NAME),
                        kv("jobId", job.getId()),
                        kv("requestId", requestId));
        }

        return Collections.emptyList();
    }
    

    private List<String> getTextData(JobDTO job, String requestId) {
        if (job.getResults().isEmpty()) {
            throw new ResourceNotFoundException();
        }

        Map<String, String> params = job.getResults().get(0).getResultStoreParams();
        String bucket = params.get(BUCKET_NAME_KEY);
        String object = params.get(OBJECT_NAME_KEY);
        String objectPrefix = params.get(OBJECT_PREFIX_KEY);
        String metadataObject = params.get(METADATA_OBJECT_NAME_KEY);

        List<byte[]> allContents = new ArrayList<>();
        if (StringUtils.isNotEmpty(bucket)) {
            if (StringUtils.isNotEmpty(object)) {
                byte[] contents = s3Service.getObject(bucket, object, requestId);
                allContents.add(contents);
            } else if (StringUtils.isNotEmpty(objectPrefix)) {
                List<byte[]> contentsList = s3Service.getAllObjects(bucket, objectPrefix, requestId).stream()
                        .filter(objectInfoPair -> !objectInfoPair.getFirst().equals(metadataObject))
                        .map(Pair::getSecond)
                        .collect(Collectors.toList());

                allContents.addAll(contentsList);
            }
        }

        return allContents.stream()
                .map(data -> new String(data, StandardCharsets.UTF_8))
                .collect(Collectors.toList());
    }
}
