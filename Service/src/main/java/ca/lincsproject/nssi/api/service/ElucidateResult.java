package ca.lincsproject.nssi.api.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Map;

@Data
@Builder // FIXME when IntelliJ patches lombok bug
@NoArgsConstructor
@AllArgsConstructor
public class ElucidateResult implements ProcessingResult {
    /**
     * The date that these results were generated.
     */
    private LocalDateTime processingDate;

    /**
     * Metadata associated with the results.
     */
    private Map<String, String> metadata;

    /**
     * A URI for the container containing the result annotations.
     */
    private String containerURI;
}
