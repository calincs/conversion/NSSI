package ca.lincsproject.nssi.api.service;

import ca.lincsproject.nssi.api.APIModuleConstants;
import ca.lincsproject.nssi.job.model.JobDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Profile(APIModuleConstants.PROFILE_NAME)
@Service
public class ElucidateResultsService {

    @Autowired
    ElucidateResultsService() {
    }

    public ElucidateResult getResults(JobDTO job) {
        // Metadata is a possible TODO

        return ElucidateResult.builder()
                .processingDate(job.getLastUpdatedTime())
                .containerURI(job.getResultsUri())
                .metadata(Collections.emptyMap())
                .build();
    }

    public void deleteResults(JobDTO job) {
        // Nothing to do; Elucidate does not support deletes.
    }
}
