package ca.lincsproject.nssi.api.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Data
@Builder // FIXME when IntelliJ patches lombok bug
@NoArgsConstructor
@AllArgsConstructor
public class ReconciliationJsonResult implements S3Result {
    /**
     * The processing date for these results.
     */
    private LocalDateTime processingDate;

    /**
     * Metadata associated with the results.
     */
    private Map<String, String> metadata;

    /**
     * Maps containing the result data.
     */
    private List<List<Map<String, Object>>> data;
}
