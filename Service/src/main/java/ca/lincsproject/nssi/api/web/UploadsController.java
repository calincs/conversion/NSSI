package ca.lincsproject.nssi.api.web;

import ca.lincsproject.nssi.api.service.UploadsService;
import ca.lincsproject.nssi.api.APIModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * This controller provides NSSI API endpoints related to managing uploads.
 */

@Profile(APIModuleConstants.PROFILE_NAME)
@RestController
public class UploadsController {

    private final UploadsService uploadsService;

    private final Logger LOGGER = LoggerFactory.getLogger(UploadsController.class);

    @Autowired
    UploadsController(UploadsService uploadsService)
    {
        this.uploadsService = uploadsService;
    }

     /**
     * Gets the specified uploaded file.
     *
     * @param uploadId the ID of the uploaded file
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    @GetMapping(value = "/api/uploads/{uploadId}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> downloadFile(@PathVariable("uploadId") Long uploadId,
                                               @RequestHeader("requestId") String requestId) {

        return uploadsService.retrieve(uploadId, requestId)
                .map(file -> {
                    HttpHeaders responseHeaders = new HttpHeaders();
                    responseHeaders.setContentDisposition(
                            ContentDisposition.builder("attachment")
                                    .filename(file.getOriginalName())
                                    .build()
                    );

                    return ResponseEntity.ok()
                            .headers(responseHeaders)
                            .body(file.getFileData());
                })
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * Deletes the specified uploaded file.
     *
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param uploadId the ID of the uploaded file
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    @DeleteMapping(value = "/api/uploads/{uploadId}")
    public ResponseEntity deleteFile(Principal principal,
                                     @PathVariable("uploadId") Long uploadId,
                                     @RequestHeader("Authorization") String authToken,
                                     @RequestHeader("requestId") String requestId)
    {
        LOGGER.info("Deleting Uploaded File",  
                   kv("service", APIModuleConstants.SERVICE_NAME),
                   kv("uploadId", uploadId),
                   kv("user", principal.getName()),
                   kv("requestId", requestId));

        uploadsService.delete(uploadId, requestId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
