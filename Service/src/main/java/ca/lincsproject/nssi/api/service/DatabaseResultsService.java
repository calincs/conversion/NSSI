package ca.lincsproject.nssi.api.service;

import ca.lincsproject.nssi.api.APIModuleConstants;
import ca.lincsproject.nssi.job.model.JobDTO;
import ca.lincsproject.nssi.job.model.ResultDTO;
import ca.lincsproject.nssi.job.service.JobService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Profile(APIModuleConstants.PROFILE_NAME)
@Service
public class DatabaseResultsService {
    private final JobService jobService;
    private final ObjectMapper objectMapper;

    private final TypeReference<Map<String, Object>> OBJ_MAP_TYPEREF = new TypeReference<>() {
    };

    private final Logger LOGGER = LoggerFactory.getLogger(DatabaseResultsService.class);

    @Autowired
    DatabaseResultsService(JobService jobService,
                           ObjectMapper objectMapper) {
        this.jobService = jobService;
        this.objectMapper = objectMapper;
    }

    public EntityJsonResult getResults(JobDTO job) {
        List<Map<String, Object>> results = new ArrayList<>();

        for (ResultDTO resultDTO : job.getResults()) {
            try {
                Map<String, Object> result = objectMapper.readValue(resultDTO.getDocument(), OBJ_MAP_TYPEREF);
                results.add(result);
            } catch (Exception ignored) {

            }
        }

        EntityJsonResult jsonResult = EntityJsonResult.builder()
                .data(results)
                .build();

        return jsonResult;
    }

    public void deleteResults(JobDTO job) {
        for (ResultDTO result : job.getResults()) {
            jobService.deleteResult(job.getId(), result.getId());
        }
    }
}
