package ca.lincsproject.nssi.api.service;

import ca.lincsproject.nssi.api.APIModuleConstants;
import ca.lincsproject.nssi.api.repository.ProtectedResourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile(APIModuleConstants.PROFILE_NAME)
@Service
public class ProtectedResourceService {

    private final ProtectedResourceRepository resourceRepository;

    @Autowired
    ProtectedResourceService(ProtectedResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }

    public void registerUploadedFile(String fileName, String ownerId, String uri) {
        resourceRepository.createProtectedResource(
                fileName,
                "Uploaded file: " + fileName,
                "urn:nssi:upload",
                ownerId,
                uri
        );
    }

    public void registerResults(Long jobId, String ownerId, String uri) {
        resourceRepository.createProtectedResource(
                "Job " + jobId.toString() + " Results",
                "NSSI Results for Job " + jobId,
                "urn:nssi:result",
                ownerId,
                uri
        );
    }
}

