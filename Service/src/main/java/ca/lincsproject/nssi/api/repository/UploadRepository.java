package ca.lincsproject.nssi.api.repository;

import ca.lincsproject.nssi.api.APIModuleConstants;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Profile(APIModuleConstants.PROFILE_NAME)
@Repository
public interface UploadRepository extends JpaRepository<Upload, Long> {
}
