package ca.lincsproject.nssi.api.service;

import ca.lincsproject.nssi.api.APIModuleConstants;
import ca.lincsproject.nssi.api.repository.Upload;
import ca.lincsproject.nssi.api.repository.UploadRepository;
import ca.lincsproject.nssi.s3_api.service.LincsS3Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
@Profile(APIModuleConstants.PROFILE_NAME)
public class UploadsService {
    private final UploadRepository repository;
    private final LincsS3Service s3Service;
    private final String uploadedBaseURI;

    private final String appDataBucket;
    private final String objectNameBase = "nssi/uploads/";
    private final Logger LOGGER = LoggerFactory.getLogger(UploadsService.class);

    @Autowired
    UploadsService(UploadRepository repository,
                   LincsS3Service s3Service,
                   @Value("${nssi.api.upload_base_url}") String uploadedBaseURI,
                   @Value("${nssi.s3.apps-data-bucket}") String appDataBucket)
    {
        this.repository = repository;
        this.s3Service = s3Service;
        this.uploadedBaseURI = uploadedBaseURI;
        this.appDataBucket = appDataBucket;
    }

    public String save(MultipartFile file, String user, String requestId) throws IOException {

        String object = objectNameBase + user + "/" + UUID.randomUUID();
        s3Service.putObject(appDataBucket, object, file.getBytes(), requestId);

        Upload upload = Upload.builder()
                .originalName(encodeAndTruncate(file.getOriginalFilename()))
                .contentType(file.getContentType())
                .uploadedTime(LocalDateTime.now())
                .uploadedBy(user)
                .s3ObjectName(object)
                .build();

        repository.save(upload);

        return uploadedBaseURI + upload.getId().toString();
    }

    public Optional<Upload> retrieve(Long uploadId, String requestId) {
        return repository.findById(uploadId)
                .map(upload -> {
                    byte[] fileData = s3Service.getObject(appDataBucket, upload.getS3ObjectName(), requestId);
                    return upload.toBuilder().fileData(fileData).build();
                });
    }

    public void delete(Long uploadId, String requestId) {
        repository.findById(uploadId).ifPresent(
                upload -> {
                    s3Service.deleteObject(appDataBucket, upload.getS3ObjectName(), requestId);
                }
        );

        repository.deleteById(uploadId);
    }

    private String encodeAndTruncate(String fileName) {
        String encodedFileName = fileName;

        encodedFileName = URLEncoder.encode(encodedFileName, StandardCharsets.UTF_8);

        return encodedFileName
                .substring(0, Math.min(encodedFileName.length(), 300))
                .replaceAll("\\s", "");
    }
}
