package ca.lincsproject.nssi.api.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "uploads")
public class Upload {
    /**
     * The upload ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Original name of the file.
     */
    private String originalName;

    /**
     * The content type of the file.
     */
    private String contentType;

    /**
     * Timestamp of when the file was uploaded.
     */
    private LocalDateTime uploadedTime;

    /**
     * User ID of the user who submitted the upload request.
     */
    private String uploadedBy;

    /**
     * Location of the file on S3.
     */
    private String s3ObjectName;

    /**
     * The file contents.
     */
    private byte[] fileData;
}
