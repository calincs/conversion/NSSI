package ca.lincsproject.nssi.api.web;

import ca.lincsproject.nssi.api.APIModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.security.Principal;
import java.util.Optional;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * This controller provides a proxy to the Elucidate API through NSSI.
 *
 * Since Elucidate's security model differs from the one we are using in
 * NSSI/LINCS, we don't expose the Elucidate API directly to end-users.
 * Instead, all requests related to annotations or annotation containers
 * are sent to NSSI and handled by this controller, which passes the request
 * onto Elucidate, and returns the results it receives from Elucidate to the
 * user.
 *
 * For more details on the Elucidate API, see https://github.com/dlcs/elucidate-server/blob/master/USAGE.md.
 */

@Profile(APIModuleConstants.PROFILE_NAME)
@RestController
public class ElucidateController {
    private final RestTemplate restTemplate;
    private final String elucidateContainerBaseUrl;
    private final String elucidateAnnotationBaseUrl;

    private final Logger LOGGER = LoggerFactory.getLogger(ElucidateController.class);

    @Autowired
    ElucidateController(@Value("${annotation-server.base_url}") String elucidateBaseUrl,
                        RestTemplate restTemplate)
    {
        this.elucidateContainerBaseUrl = elucidateBaseUrl + "/{spec}/{containerId}/";
        this.elucidateAnnotationBaseUrl = elucidateBaseUrl + "/{spec}/{containerId}/{annotationId}";
        this.restTemplate = restTemplate;
    }

    /**
     * Returns the specified annotation container.
     * 
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     *
     * See https://github.com/dlcs/elucidate-server/blob/master/USAGE.md for an
     * explanation of the other parameters.
     */
    @GetMapping("/api/elucidate/annotation/{spec}/{containerId}")
    public ResponseEntity getAnnotationResults(Principal principal,
                                               @PathVariable("spec") String specification,
                                               @PathVariable("containerId") String containerId,
                                               @RequestParam("page") Optional<Long> pageNum,
                                               @RequestParam("desc") Optional<Integer> order,
                                               @RequestHeader("Authorization") String authToken,
                                               @RequestHeader("requestId") String requestId,
                                               @RequestBody Optional<String> requestBody)
    {
        try {
            var requestUriBuilder = UriComponentsBuilder.fromUriString(elucidateContainerBaseUrl);
            
            // TODO: change these to use `ifPresent`
            pageNum.map(n -> requestUriBuilder.queryParam("page", n));
            order.map(o -> requestUriBuilder.queryParam("desc", o));

            String requestUri = requestUriBuilder.buildAndExpand(specification, containerId).toUriString();
            HttpEntity<String> request = new HttpEntity<>(requestBody.orElse(null));

            return restTemplate.exchange(requestUri, HttpMethod.GET, request, String.class);
        } catch (Exception e) {
            LOGGER.error("FAILED To Retrieve Annotation Results",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", APIModuleConstants.SERVICE_NAME),
                        kv("container", containerId),
                        kv("requestId", requestId));
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Returns the specified annotation.
     * 
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     *
     * See https://github.com/dlcs/elucidate-server/blob/master/USAGE.md for an
     * explanation of the other parameters.
     */
    @GetMapping(value = "/api/elucidate/annotation/{spec}/{containerId}/{annotationId}")
    public ResponseEntity getAnnotation(Principal principal,
                                        @PathVariable("spec") String specification,
                                        @PathVariable("containerId") String containerId,
                                        @PathVariable("annotationId") String annotationId,
                                        @RequestHeader("Authorization") String authToken,
                                        @RequestHeader("requestId") String requestId,
                                        @RequestBody Optional<String> requestBody)
    {
        try {
            var requestUriBuilder = UriComponentsBuilder.fromUriString(elucidateAnnotationBaseUrl);

            String requestUri = requestUriBuilder
                    .buildAndExpand(specification, containerId, annotationId)
                    .toUriString();
            HttpEntity<String> request = new HttpEntity<>(requestBody.orElse(null));

            return restTemplate.exchange(requestUri, HttpMethod.GET, request, String.class);
        } catch (Exception e) {
            LOGGER.error("FAILED To Retrieve Annotation",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", APIModuleConstants.SERVICE_NAME),
                        kv("container", containerId),
                        kv("annotation", annotationId),
                        kv("requestId", requestId));
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds a new annotation into the specified annotation container.
     * 
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     *
     * See https://github.com/dlcs/elucidate-server/blob/master/USAGE.md for an
     * explanation of the other parameters.
     */
    @PostMapping(value = "/api/elucidate/annotation/{spec}/{containerId}")
    public ResponseEntity createAnnotation(Principal principal,
                                           @PathVariable("spec") String specification,
                                           @PathVariable("containerId") String containerId,
                                           @RequestHeader("Authorization") String authToken,
                                           @RequestHeader("requestId") String requestId,
                                           @RequestBody Optional<String> requestBody)
    {
        try {
            var requestUriBuilder = UriComponentsBuilder.fromUriString(elucidateContainerBaseUrl);

            String requestUri = requestUriBuilder
                    .buildAndExpand(specification, containerId)
                    .toUriString();
            HttpEntity<String> request = new HttpEntity<>(requestBody.orElse(null));

            return restTemplate.exchange(requestUri, HttpMethod.POST, request, String.class);
        } catch (Exception e) {
            LOGGER.error("FAILED To Create Annotation",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", APIModuleConstants.SERVICE_NAME),
                        kv("container", containerId),
                        kv("requestId", requestId));
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Updates the specified annotation.
     * 
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     *
     * See https://github.com/dlcs/elucidate-server/blob/master/USAGE.md for an
     * explanation of the other parameters.
     */
    @PutMapping(value = "/api/elucidate/annotation/{spec}/{containerId}/{annotationId}")
    public ResponseEntity updateAnnotation(Principal principal,
                                           @PathVariable("spec") String specification,
                                           @PathVariable("containerId") String containerId,
                                           @PathVariable("annotationId") String annotationId,
                                           @RequestHeader("Authorization") String authToken,
                                           @RequestHeader("requestId") String requestId,
                                           @RequestHeader("If-Match") String resourceETag,
                                           @RequestBody Optional<String> requestBody)
    {
        try {
            var requestUriBuilder = UriComponentsBuilder.fromUriString(elucidateAnnotationBaseUrl);

            String requestUri = requestUriBuilder
                    .buildAndExpand(specification, containerId, annotationId)
                    .toUriString();

            var headers = new LinkedMultiValueMap<String, String>();
            headers.add("If-Match", resourceETag);
            headers.add("Accept", "application/ld+json");
            headers.add("Content-Type", "application/ld+json");
            HttpEntity<String> request = new HttpEntity<>(requestBody.orElse(null), headers);

            return restTemplate.exchange(requestUri, HttpMethod.PUT, request, String.class);
        } catch (Exception e) {
            LOGGER.error("FAILED To Update Annotation",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", APIModuleConstants.SERVICE_NAME),
                        kv("container", containerId),
                        kv("annotation", annotationId),
                        kv("requestId", requestId));
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes the specified annotation.
     * 
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     *
     * See https://github.com/dlcs/elucidate-server/blob/master/USAGE.md for an
     * explanation of the other parameters.
     */
    @DeleteMapping(value = "/api/elucidate/annotation/{spec}/{containerId}/{annotationId}")
    public ResponseEntity deleteAnnotation(Principal principal,
                                           @PathVariable("spec") String specification,
                                           @PathVariable("containerId") String containerId,
                                           @PathVariable("annotationId") String annotationId,
                                           @RequestHeader("Authorization") String authToken,
                                           @RequestHeader("requestId") String requestId,
                                           @RequestHeader("If-Match") String resourceETag,
                                           @RequestBody Optional<String> requestBody)
    {
        try {
            var requestUriBuilder = UriComponentsBuilder.fromUriString(elucidateAnnotationBaseUrl);

            String requestUri = requestUriBuilder
                    .buildAndExpand(specification, containerId, annotationId)
                    .toUriString();

            var headers = new LinkedMultiValueMap<String, String>();
            headers.add("If-Match", resourceETag);
            HttpEntity<String> request = new HttpEntity<>(requestBody.orElse(null), headers);

            return restTemplate.exchange(requestUri, HttpMethod.DELETE, request, String.class);
        } catch (Exception e) {
            LOGGER.error("FAILED To Delete Annotation",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", APIModuleConstants.SERVICE_NAME),
                        kv("container", containerId),
                        kv("annotation", annotationId),
                        kv("requestId", requestId));
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
