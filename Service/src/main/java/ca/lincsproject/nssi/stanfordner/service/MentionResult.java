package ca.lincsproject.nssi.stanfordner.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MentionResult {
    /**
     * The type of entity mention (e.g., PERSON, ORGANIZATION, etc).
     */
    private String type;

    /**
     * The string constituting the entity mention (e.g., "Victor Hugo").
     */
    private String value;

    /**
     * The character position of the start of the entity mention in the text.
     */
    private int startPosition;

    /**
     * The character position of the end of the entity mention in the text.
     */
    private int endPosition;
}
