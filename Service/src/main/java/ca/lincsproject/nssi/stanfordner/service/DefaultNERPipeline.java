package ca.lincsproject.nssi.stanfordner.service;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class DefaultNERPipeline implements NERPipeline {
    private final Properties props;
    private final StanfordCoreNLP pipeline;
    private final XMLProcessorUtils xmlUtils;

    private final List<String> posTags = List.of("NN", "NNP", "NNPS");
    private final List<String> entityTypeTags = List.of("PERSON", "LOCATION", "ORGANIZATION", "MISC");

    private final static Logger LOGGER = LoggerFactory.getLogger(CorefNERPipeline.class);

    public DefaultNERPipeline(XMLProcessorUtils xmlUtils)
            throws ClassCastException
    {
        this.xmlUtils = xmlUtils;

        LOGGER.info("initializing default pipeline");

        props = new Properties();
        props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner");
        props.setProperty("ner.applyFineGrained", "false");
        props.setProperty("ner.applyNumericClassifiers", "false");

        pipeline = new StanfordCoreNLP(props);

        LOGGER.info("default pipeline created");
    }

    public List<CorrelatedNERResults> run(String input, Integer offset) {
        CoreDocument doc = new CoreDocument(input);
        pipeline.annotate(doc);

        var filteredEntityMentions = doc.entityMentions().stream()
                .filter(em -> {
                    var tokensAnnotations = em.coreMap().get(CoreAnnotations.TokensAnnotation.class);
                    var pos = tokensAnnotations.get(0).get(CoreAnnotations.PartOfSpeechAnnotation.class);
                    return posTags.contains(pos);
                })
                .filter(em -> entityTypeTags.contains(em.entityType()))
                .collect(Collectors.toList());

        List<CorrelatedNERResults> results = filteredEntityMentions.stream()
                .filter(mention -> mention.canonicalEntityMention().isPresent())
                .map(mention ->
                        CorrelatedNERResults.builder()
                                .representative(mention.text())
                                .entityType(mention.entityType())
                                .allMatch(EntityMatch.builder()
                                        .entityMention(mention.text())
                                        .startPosition(mention.charOffsets().first() + offset)
                                        .endPosition(mention.charOffsets().second() + offset)
                                        .build()
                                )
                                .build()
                )
                .collect(Collectors.toList());

        return results;
    }

    public static void main(String[] args) {
        String s = "Port au Prince is the capital of Haiti.";

        DefaultNERPipeline p = new DefaultNERPipeline(null);

        var r = p.run(s, 0);

        System.out.println("hello!");
    }
}
