package ca.lincsproject.nssi.stanfordner.infrastructure.config;


import ca.lincsproject.nssi.stanfordner.StanfordNERModuleConstants;
import ca.lincsproject.nssi.stanfordner.service.CorefNERPipeline;
import ca.lincsproject.nssi.stanfordner.service.DefaultNERPipeline;
import ca.lincsproject.nssi.stanfordner.service.XMLProcessorUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static ca.lincsproject.nssi.broker.infrastructure.config.RMQConfiguration.APP_MESSAGES_EXCHANGE;
import static ca.lincsproject.nssi.broker.infrastructure.config.RMQConfiguration.DLQ_KEY;
import static ca.lincsproject.nssi.broker.infrastructure.config.RMQConfiguration.DLX_APP_MESSAGES;


@Profile(StanfordNERModuleConstants.PROFILE_NAME)
@Configuration
public class StanfordNERConfiguration {

    @Bean
    public DefaultNERPipeline defaultPipeline(XMLProcessorUtils xmlUtils) {
        return new DefaultNERPipeline(xmlUtils);
    }

    @Bean
    public CorefNERPipeline corefPipeline(XMLProcessorUtils xmlUtils)
    {
        return new CorefNERPipeline(xmlUtils);
    }

    @Bean
    public Queue stanfordNERQueue(@Value("${nssi.processing.services.stanfordner.jobQueue}") String queueName) {
        return QueueBuilder.durable(queueName)
                .withArgument(DLQ_KEY, DLX_APP_MESSAGES)
                .build();
    }

    @Bean
    public Binding stanfordNERBinding(@Qualifier(APP_MESSAGES_EXCHANGE) DirectExchange exchange,
                                      @Value("${nssi.processing.services.stanfordner.jobQueue}") String queueName) {
        return BindingBuilder.bind(stanfordNERQueue(queueName)).to(exchange)
                .with(queueName);
    }

    @Bean
    public XMLProcessorUtils xmlProcessorUtils(ObjectMapper objectMapper) {
        return new XMLProcessorUtils(objectMapper);
    }
}