package ca.lincsproject.nssi.stanfordner.service;

import ca.lincsproject.nssi.broker.message.NSSIServiceMessage;
import ca.lincsproject.nssi.broker.service.RMQUtilsService;
import ca.lincsproject.nssi.standoff.model.Annotation;
import ca.lincsproject.nssi.standoff.service.StandoffConverter;
import ca.lincsproject.nssi.standoff.service.StandoffDocument;
import ca.lincsproject.nssi.stanfordner.StanfordNERModuleConstants;
import edu.stanford.nlp.util.Interval;
import edu.stanford.nlp.util.IntervalTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(StanfordNERModuleConstants.PROFILE_NAME)
@Service
public class XMLProcessor {
  private final StandoffConverter standoffConverter;
  private final XMLProcessorUtils xmlProcessorUtils;
  private final RMQUtilsService rmqUtils;

  private final String ORGANIZATION_TAG_KEY = "ORGANIZATION_TAG";
  private final String LOCATION_TAG_KEY = "LOCATION_TAG";
  private final String PERSON_TAG_KEY = "PERSON_TAG";
  private final String TITLE_TAG_KEY = "TITLE_TAG";

  private final Logger LOGGER = LoggerFactory.getLogger(XMLProcessor.class);

  @Autowired
  public XMLProcessor(StandoffConverter standoffConverter,
      XMLProcessorUtils xmlProcessorUtils,
      RMQUtilsService rmqUtils) {
    this.standoffConverter = standoffConverter;
    this.xmlProcessorUtils = xmlProcessorUtils;
    this.rmqUtils = rmqUtils;
  }

  public void handleMessage(NSSIServiceMessage message,
      String document,
      String headerTagName,
      NERPipeline classifier) {
    try {
      StandoffDocument standoffDocument = createStandoffDocument(document);
      Annotation header = standoffDocument.findFirst(headerTagName).get();
      IntervalTree<Integer, Interval<Integer>> intervalsToIgnore = new IntervalTree<>();

      Map<String, String> context = message.getContext();
      if (context != null) {
        LOGGER.info("NSSIServiceMessage context size: " + context.size());
        List<String> entityTags = List.of(
            context.getOrDefault(ORGANIZATION_TAG_KEY, ""),
            context.getOrDefault(LOCATION_TAG_KEY, ""),
            context.getOrDefault(PERSON_TAG_KEY, ""),
            context.getOrDefault(TITLE_TAG_KEY, "")
          ).stream().filter(Objects::nonNull).collect(Collectors.toList());
        standoffDocument.getAnnotations().stream()
            .filter(annotation -> entityTags.contains(annotation.getLabel()))
            .filter(annotation -> annotation.getStart() > header.getEnd())
            .forEach(
                annotation -> intervalsToIgnore.add(Interval.toInterval(annotation.getStart(), annotation.getEnd())));
      } else {
        LOGGER.error("NSSIServiceMessage context is NULL. Cannot get Entity Tags!");
        standoffDocument.getAnnotations().stream()
            .filter(annotation -> annotation.getStart() > header.getEnd())
            .forEach(
                annotation -> intervalsToIgnore.add(Interval.toInterval(annotation.getStart(), annotation.getEnd())));
      }

      Annotation textAnno = standoffDocument.findFirst("text").get();
      List<Annotation> sections = standoffDocument.getAnnotations().stream()
          .filter(annotation -> annotation.getStart() >= textAnno.getStart())
          .filter(annotation -> "p".equalsIgnoreCase(annotation.getLabel()))
          .collect(Collectors.toList());

      LOGGER.info("CLASSIFIER: # of sections: " + sections.size());

      String text = standoffDocument.getPlain();
      for (Annotation annotation : sections) {
        LOGGER.info("CLASSIFIER: Processing section from " + annotation.getStart() + " to " + annotation.getEnd());
        String annotationText = text.substring(annotation.getStart(), annotation.getEnd());
        var result = classifier.run(
            annotationText,
            0).stream()
            .map(r -> r.filterMatchesByOverlap(intervalsToIgnore))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .peek(r -> r.getAllMatches().forEach(match -> {
              standoffDocument.findContaining(
                  match.getStartPosition() + annotation.getStart(),
                  match.getEndPosition() + annotation.getStart()).ifPresent(matchAnnotation -> {
                    String matchXpath = standoffDocument.getXPathFromAnnotation(matchAnnotation);
                    LOGGER.debug(matchXpath);
                    match.setEntityXpath(matchXpath);
                  });
            }))
            .collect(Collectors.toList());

        sendNamedEntityMessages(message, result);
      }
    } catch (Exception e) {
      LOGGER.error("FAILED To Process XML File",
          kv("exception", e.getMessage()),
          kv("stackTrace", e.getStackTrace()));
    }
  }

  private StandoffDocument createStandoffDocument(String document)
      throws ParserConfigurationException, IOException, SAXException {
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = dbf.newDocumentBuilder();
    String cleanedDocument = document.replaceAll("<\\?.*?\\?>", "").trim();
    Document teiDocument = builder.parse(new InputSource(new StringReader(cleanedDocument)));
    return standoffConverter.toStandoff(teiDocument);
  }

  private void sendNamedEntityMessages(NSSIServiceMessage message,
      List<CorrelatedNERResults> classifierResults) {
    if (classifierResults.size() == 0) {
      LOGGER.info("No entities found.");
    } else {
      var namedEntities = xmlProcessorUtils.getNamedEntityInfo(classifierResults);
      var resultMessages = xmlProcessorUtils.convertNamedEntityResults(namedEntities, message);

      resultMessages.forEach(m -> rmqUtils.sendToNextStep(StanfordNERService.class, m));

      LOGGER.info("Sending Progress Message",
          kv("size", classifierResults.size()));
    }
  }
}
