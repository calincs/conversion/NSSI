package ca.lincsproject.nssi.stanfordner.service;

import ca.lincsproject.nssi.broker.message.EntityClassification;
import ca.lincsproject.nssi.broker.message.NSSIServiceMessage;
import ca.lincsproject.nssi.broker.message.NamedEntityInfo;
import ca.lincsproject.nssi.broker.message.Selection;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static net.logstash.logback.argument.StructuredArguments.kv;

public class XMLProcessorUtils {
    private final ObjectMapper objectMapper;

    private final Logger LOGGER = LoggerFactory.getLogger(XMLProcessorUtils.class);

    @Autowired
    public XMLProcessorUtils(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public String cleanLemma(String lemma)
    {
        String entity = StringUtils.normalizeSpace(lemma.replaceAll("\"", ""));
        return entity;
    }

    public List<NamedEntityInfo> getNamedEntityInfo(List<CorrelatedNERResults> classifierResults) {
        return classifierResults.stream()
                .map(result -> {
                    String originalClassLabel = result.getEntityType();
                    EntityClassification classification = EntityClassification.MISC;
                    try {
                        classification = EntityClassification.valueOf(originalClassLabel);
                    } catch (IllegalArgumentException ignored) {

                    }

                    NamedEntityInfo.Builder neInfoBuilder = NamedEntityInfo.builder()
                            .entity(result.getRepresentative())
                            .classification(classification);

                    result.getAllMatches().forEach(match ->
                            neInfoBuilder.selection(new NamedEntityInfo.SelectionWithLemma(
                                            match.getEntityMention(),
                                            Selection.builder()
                                                    .xpath(match.getEntityXpath())
                                                    .start(match.getStartPosition())
                                                    .end(match.getEndPosition())
                                                    .build()
                                    )
                            )
                    );
                    return neInfoBuilder.build();
                })
                .collect(Collectors.toList());
    }

    public List<NSSIServiceMessage> convertNamedEntityResults(List<NamedEntityInfo> namedEntities,
                                                              NSSIServiceMessage message)
    {
        String neInfoDocument = "";

        List<NSSIServiceMessage> result = new ArrayList<>();
        for (NamedEntityInfo namedEntity : namedEntities) {
            try {
                neInfoDocument = objectMapper.writeValueAsString(namedEntity);
            } catch (JsonProcessingException e) {
                LOGGER.error("FAILED To Process Info Document",
                        kv("stackTrace", e.getStackTrace()));
            }

            NSSIServiceMessage resultMessage = NSSIServiceMessage.builder()
                    .jobId(message.getJobId())
                    .pipeline(message.getPipeline())
                    .resultsUri(message.getResultsUri())
                    .document(neInfoDocument)
                    .documentURI(message.getDocumentURI())
                    .format(message.getFormat())
                    .authorities(message.getAuthorities())
                    .context(message.getContext())
                    .requestId(message.getRequestId())
                    .build();

            result.add(resultMessage);
        }

        return result;
    }
}