package ca.lincsproject.nssi.stanfordner.service;

import edu.stanford.nlp.util.Interval;
import edu.stanford.nlp.util.IntervalTree;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CorrelatedNERResults {
    /**
     * The XPath for the element immediately containing the entity.
     */
    private String xpath;

    /**
     * The most representative string for all of the entity mentions encompassed
     * in this set of correlated entities.
     */
    private String representative;

    /**
     * The entity type that applies to all entities within this set of correlated
     * entities.
     */
    private String entityType;

    /**
     * A list of correlated entities in the form of (entity mention, (start position,
     * end position)) pairs.
     */
    @Singular
    private List<EntityMatch> allMatches;

    /**
     * Returns all matches that occur after the given {@code endPosition}.
     *
     * @param endPosition the position after which to look for entities
     */
    public Optional<CorrelatedNERResults> filterMatchesByEndPosition(int endPosition) {
        allMatches = allMatches.stream().filter(m -> m.getEndPosition() > endPosition)
                .collect(Collectors.toList());

        if (allMatches.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(this);
    }

    /**
     * Returns all matches that do not overlap with the start and end positions
     * given in {@code intervalsToIgnore}
     * @param intervalsToIgnore a collection of intervals which demarcate sections
     *                          of the text to ignore.
     */
    public Optional<CorrelatedNERResults> filterMatchesByOverlap(
            IntervalTree<Integer, Interval<Integer>> intervalsToIgnore
    )
    {
        allMatches = allMatches.stream().filter(m -> !intervalsToIgnore.overlaps(
                Interval.toInterval(m.getStartPosition(), m.getEndPosition()))
        ).collect(Collectors.toList());

        if (allMatches.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(this);
    }
}
