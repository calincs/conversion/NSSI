package ca.lincsproject.nssi.stanfordner.service;

import java.util.List;

public interface NERPipeline {
    List<CorrelatedNERResults> run(String output, Integer offset);
}
