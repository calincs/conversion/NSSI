package ca.lincsproject.nssi.core.infrastructure.config;

import com.hazelcast.config.*;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.List;
import java.util.stream.Collectors;

@Configuration
public class CacheConfiguration {

    @Bean
    @Profile({"docker", "sandbox"})
    public NetworkConfig networkConfigLocal() {
        NetworkConfig networkConfig = new NetworkConfig();
        networkConfig.getJoin().getMulticastConfig().setEnabled(true);

        return networkConfig;
    }

    @Bean
    @Profile({"stage", "prod"})
    public NetworkConfig networkConfig(NSSIHazelcastConfigProperties cacheConfig) {
        NetworkConfig networkConfig = new NetworkConfig();
        networkConfig.getJoin().getKubernetesConfig()
                .setEnabled(true)
                .setProperty("service-dns", cacheConfig.getServiceDNS());

        return networkConfig;
    }

    @Bean
    public Config hazelcastConfig(NetworkConfig networkConfig,
                                  NSSIHazelcastConfigProperties cacheConfig)
    {
        Config config = new Config();
        config.setNetworkConfig(networkConfig);

        EvictionConfig evictionConfig = new EvictionConfig();
        evictionConfig.setEvictionPolicy(EvictionPolicy.LRU)
                .setSize(cacheConfig.getMaxCacheSize())
                .setMaxSizePolicy(MaxSizePolicy.FREE_HEAP_SIZE);

        List<MapConfig> mapConfigs = cacheConfig.getCaches().stream()
                .map(cacheInfo -> new MapConfig()
                        .setName(cacheInfo.getName())
                        .setBackupCount(cacheInfo.getBackup())
                        .setTimeToLiveSeconds(cacheInfo.getTtlSeconds())
                        .setEvictionConfig(evictionConfig))
                .collect(Collectors.toList());

        mapConfigs.forEach(config::addMapConfig);

        return config;
    }

    @Bean
    public HazelcastInstance hazelcastInstance(Config config) {
        return Hazelcast.newHazelcastInstance(config);
    }

    @Bean
    public CacheManager cacheManager(HazelcastInstance hazelcastInstance) {
        return new HazelcastCacheManager(hazelcastInstance);
    }
}
