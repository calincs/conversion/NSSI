package ca.lincsproject.nssi.annotation.service;

import ca.lincsproject.nssi.broker.message.EntityClassification;

import java.util.Map;

public class AnnotationConstants {
    // Language codes
    public static final String LANGUAGE_EN = "en";

    // Schemas
    public static final String RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    public static final String RDFS = "http://www.w3.org/2000/01/rdf-schema#";
    public static final String AS = "http://www.w3.org/ns/activitystreams#";
    public static final String CWRC = "http://sparql.cwrc.ca/ontologies/cwrc#";
    public static final String BF = "http://id.loc.gov/ontologies/bibframe/";
    public static final String DC = "http://purl.org/dc/elements/1.1/";
    public static final String DCTERMS = "http://purl.org/dc/terms/";
    public static final String OA = "http://www.w3.org/ns/oa#";
    public static final String SCHEMA = "http://schema.org/";
    public static final String XSD = "http://www.w3.org/2001/XMLSchema#";

    // OA Classes
    public static final String MOTIVATION_TYPE = "oa:Motivation";
    public static final String ANNOTATION_TYPE = "oa:Annotation";
    public static final String XPATH_SELECTOR_TYPE = "oa:XPathSelector";
    public static final String TEXT_POSITION_SELECTOR_TYPE = "oa:TextPositionSelector";
    public static final String SPECIFIC_RESOURCE_TYPE = "oa:SpecificResource";
    public static final String CHOICE_TYPE = "oa:Choice";

    // CWRC Classes
    public static final String CWRC_PERSONAL_NAME_TYPE = "cwrc:PersonalName";
    public static final String CWRC_PLACE_TYPE = "cwrc:Place";

    // Org Classes
    public static final String FORMAL_ORG_TYPE = "org:FormalOrganization";

    // BIBFRAME Classes
    public static final String TITLE_TYPE = "bf:Title";

    // AS Classes
    public static final String APPLICATION_TYPE = "as:Application";

    // OWL Classes
    public static final String OWL_THING_TYPE = "owl:Thing";

    // Map of entity types
    public static final Map<EntityClassification, String> ENTITY_TYPE_MAP = Map.of(
            EntityClassification.PERSON, CWRC_PERSONAL_NAME_TYPE,
            EntityClassification.LOCATION, CWRC_PLACE_TYPE,
            EntityClassification.ORGANIZATION, FORMAL_ORG_TYPE,
            EntityClassification.TITLE, TITLE_TYPE
    );
}
