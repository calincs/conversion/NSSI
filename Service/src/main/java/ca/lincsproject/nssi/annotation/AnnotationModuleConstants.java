package ca.lincsproject.nssi.annotation;

public class AnnotationModuleConstants {
    public static final String PROFILE_NAME = "annotation";
    public static final String QUEUE_NAME = "annotationQueue";
    public static final String SERVICE_NAME = "Web Annotation Service";
}
