package ca.lincsproject.nssi.standoff.service;

import ca.lincsproject.nssi.standoff.model.Annotation;
import ca.lincsproject.nssi.standoff.model.ClassifiedEntity;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@Builder(toBuilder = true)
public class StandoffDocument {
    /**
     * The standoff annotations (i.e., XML elements) contained within
     * the document.
     */
    private List<Annotation> annotations;

    /**
     * The entities contained within the document.
     */
    @Singular
    private List<ClassifiedEntity> taggedEntities;

    /**
     * The plaintext version of the document.
     */
    private String plain;

    private final Logger LOGGER = LoggerFactory.getLogger(StandoffDocument.class);

    /**
     * Returns the first {@code Annotation} whose label matches {@code label}, if it exists.
     *
     * @param label the annotation label to search for
     */
    public Optional<Annotation> findFirst(String label) {
        return annotations.stream()
                .filter(annotation -> label.equals(annotation.getLabel()))
                .findFirst();
    }

    /**
     * Returns the closest containing {@code Annotation} for the given start and end positions,
     * or Optional.empty() if no such annotation exists.
     *
     * @param startPosition the start position
     * @param endPosition   the end position
     * @return an {@code Annotation} or Optional.empty()
     */
    public Optional<Annotation> findContaining(Integer startPosition, Integer endPosition) {
        List<Annotation> containers = new ArrayList<>();
        for (Annotation annotation : annotations) {
            if (annotation.getStart() <= startPosition && annotation.getEnd() >= endPosition) {
                containers.add(annotation);
            }
        }

        Collections.reverse(containers);
        if (containers.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(containers.get(0));
    }

    /**
     * Returns an XPath for the node represented by the given annotation.
     *
     * @param annotation the annotation
     */
    public String getXPathFromAnnotation(Annotation annotation) {
        List<String> xpathElements = new ArrayList<>();

        for (Annotation a : annotations.subList(0, annotation.getIndex())) {
            if (a.contains(annotation)) {
                xpathElements.add(a.getLabel() + getSiblingIndexStringFromAnnotation(a));
            }
        }

        xpathElements.add(annotation.getLabel() + getSiblingIndexStringFromAnnotation(annotation));

        return "/" + String.join("/", xpathElements);
    }

    private String getSiblingIndexStringFromAnnotation(Annotation annotation) {
        Annotation parent = null;
        for (int i = annotation.getIndex() - 1; i >= 0; i--) {
            if (annotations.get(i).contains(annotation)) {
                parent = annotations.get(i);
                break;
            }
        }

        if (Objects.isNull(parent)) {
            // hmm?
            return "";
        }

        int parentStart = parent.getStart();
        int parentEnd = parent.getEnd();
        List<Annotation> siblings = annotations.stream()
                .filter(a -> a.getStart() >= parentStart
                        && a.getEnd() <= parentEnd
                        && Objects.equals(a.getDepth(), annotation.getDepth())
                        && Objects.equals(a.getLabel(), annotation.getLabel()))
                .collect(Collectors.toList());

        if (siblings.size() == 1) {
            return "";
        }

        int xpathIndex = 1;
        for (Annotation sibling: siblings) {
            if (Objects.equals(sibling.getIndex(), annotation.getIndex())) {
                break;
            }
            xpathIndex++;
        }

        return "[" + xpathIndex + "]";
    }
}
