package ca.lincsproject.nssi.standoff.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClassifiedEntity {
    /**
     * The entity name (e.g., "Victor Hugo").
     */
    private String name;

    /**
     * The entity type (e.g., PERSON, ORGANIZATION, etc.)
     */
    private String type;
}
