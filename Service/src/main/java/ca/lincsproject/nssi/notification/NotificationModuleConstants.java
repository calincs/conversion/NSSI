package ca.lincsproject.nssi.notification;

public class NotificationModuleConstants {
    public static final String PROFILE_NAME = "notification";
    public static final String SERVICE_NAME = "Notification Service";
}
