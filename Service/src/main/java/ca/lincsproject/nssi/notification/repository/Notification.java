package ca.lincsproject.nssi.notification.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "notifications")
public class Notification {
    /**
     * The notification ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long notifId;

    /**
     * The ID of the client who is subscribed to this notification.
     */
    private String clientId;

    /**
     * The ID of the job for which this notification provides updates.
     */
    private String jobId;

    /**
     * The body of the notification.
     */
    @Column(columnDefinition="TEXT")
    private String body;

    /**
     * The notification time.
     */
    private LocalDateTime time;
}