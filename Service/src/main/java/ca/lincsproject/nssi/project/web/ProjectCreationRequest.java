package ca.lincsproject.nssi.project.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectCreationRequest {
    /**
     * The project name.
     */
    private String projectName;
}
