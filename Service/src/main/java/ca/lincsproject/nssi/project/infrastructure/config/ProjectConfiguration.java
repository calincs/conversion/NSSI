package ca.lincsproject.nssi.project.infrastructure.config;

import ca.lincsproject.nssi.project.ProjectModuleConstants;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile(ProjectModuleConstants.PROFILE_NAME)
@Configuration
public class ProjectConfiguration {
}
