package ca.lincsproject.nssi.linking.repository;


public enum LinkingServiceName {
    WIKIDATA("Wikidata", "https://www.wikidata.org/"),
    VIAF("VIAF", "https://viaf.org/"),
    LGPN2("LGPN2", "http://clas-lgpn2.classics.ox.ac.uk/"),
    GETTY("Getty ULAN", "http://vocab.getty.edu/"),
    DBPEDIA("DBPedia", "https://wiki.dbpedia.org/"),
    GEONAMES("Geonames", "https://www.geonames.org/");

    private final String serviceName;
    private final String uri;

    LinkingServiceName(String serviceName, String uri) {
        this.serviceName = serviceName;
        this.uri = uri;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getUri() {
        return uri;
    }
}
