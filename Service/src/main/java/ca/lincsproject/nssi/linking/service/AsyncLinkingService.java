package ca.lincsproject.nssi.linking.service;

import ca.lincsproject.nssi.linking.repository.LinkingServiceName;
import ca.lincsproject.nssi.broker.message.LinkingServiceResult;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public interface AsyncLinkingService {
    LinkingServiceName getName();
    CompletableFuture<Optional<LinkingServiceResult>> getLinks(String entity, String type);
}
