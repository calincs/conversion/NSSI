package ca.lincsproject.nssi.linking.service;

import ca.lincsproject.nssi.linking.repository.LGPN2Repository;
import ca.lincsproject.nssi.linking.repository.LGPN2Response;
import ca.lincsproject.nssi.linking.repository.LinkingServiceName;
import ca.lincsproject.nssi.broker.message.EntityClassification;
import ca.lincsproject.nssi.broker.message.EntityLinkDetails;
import ca.lincsproject.nssi.broker.message.LinkingServiceResult;
import ca.lincsproject.nssi.linking.LinkingModuleConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Service(value = "lgpn2Service")
public class LGPN2Service implements AsyncLinkingService {

    private final LGPN2Repository repository;
    private final long maxResults;
    private final String baseURI = "https://www.lgpn.ox.ac.uk/id/";

    @Autowired
    public LGPN2Service(LGPN2Repository repository,
                        @Value("${nssi.linking.lgpn2.maxResults:5}") long maxResults) {
        this.repository = repository;
        this.maxResults = maxResults;
    }

    @Override
    public LinkingServiceName getName() {
        return LinkingServiceName.LGPN2;
    }

    @Async
    @Override
    public CompletableFuture<Optional<LinkingServiceResult>> getLinks(String entity, String type)
    {
        if (type.equals(EntityClassification.LOCATION.name())
                || type.equals(EntityClassification.TITLE.name())) {
            return CompletableFuture.completedFuture(Optional.empty());
        }

        try {
            LGPN2Response response = repository.getResult(entity);

            List<EntityLinkDetails> matches = response.getPersons().stream().limit(maxResults)
                    .map(result -> EntityLinkDetails.builder()
                            .uri(baseURI + result.getId())
                            .heading(result.getName())
                            .description(makeDescription(result))
                            .build()
                    ).collect(Collectors.toList());

            return CompletableFuture.completedFuture(
                    LinkingServiceResult.builder()
                            .serviceName(this.getName().getServiceName())
                            .serviceURI(this.getName().getUri())
                            .matches(matches)
                            .buildOptional()
            );

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(Optional.empty());
    }

    private String makeDescription(LGPN2Response.LGPN2PersonResponse person) {
        return "Place: " + person.getPlace()
                + "\nFloruit: " + person.getNotBefore() + " to " + person.getNotAfter();
    }
}
