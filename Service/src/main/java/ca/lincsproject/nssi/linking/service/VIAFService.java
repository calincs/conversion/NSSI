package ca.lincsproject.nssi.linking.service;

import ca.lincsproject.nssi.linking.repository.LinkingServiceName;
import ca.lincsproject.nssi.linking.repository.VIAFRepository;
import ca.lincsproject.nssi.linking.repository.VIAFResponse;
import ca.lincsproject.nssi.broker.message.EntityLinkDetails;
import ca.lincsproject.nssi.broker.message.LinkingServiceResult;
import ca.lincsproject.nssi.linking.LinkingModuleConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Service(value = "viafService")
public class VIAFService implements AsyncLinkingService {

    private final VIAFRepository repository;

    @Autowired
    public VIAFService(VIAFRepository repository) {
        this.repository = repository;
    }

    @Override
    public LinkingServiceName getName() {
        return LinkingServiceName.VIAF;
    }

    @Async
    @Override
    public CompletableFuture<Optional<LinkingServiceResult>> getLinks(String entity, String type)
    {
        VIAFResponse response = repository.getResult(entity, type);

        if (response.getSearchRetrieveResponse().getNumberOfRecords() == 0) {
            return CompletableFuture.completedFuture(Optional.empty());
        }

        List<EntityLinkDetails> matches = response.getSearchRetrieveResponse().getRecords().stream()
                .map(result -> result.get("record"))
                .map(result ->
                        EntityLinkDetails.builder()
                                .uri(result.getAboutURI())
                                .heading(result.getMainHeading())
                                .build())
                .collect(Collectors.toList());

        return CompletableFuture.completedFuture(
                LinkingServiceResult.builder()
                        .serviceName(this.getName().getServiceName())
                        .serviceURI(this.getName().getUri())
                        .matches(matches)
                        .buildOptional()
        );
    }
}
