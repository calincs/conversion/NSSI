package ca.lincsproject.nssi.linking.service;

import ca.lincsproject.nssi.linking.repository.GeonamesRepository;
import ca.lincsproject.nssi.linking.repository.GeonamesResponse;
import ca.lincsproject.nssi.linking.repository.LinkingServiceName;
import ca.lincsproject.nssi.broker.message.EntityClassification;
import ca.lincsproject.nssi.broker.message.EntityLinkDetails;
import ca.lincsproject.nssi.broker.message.LinkingServiceResult;
import ca.lincsproject.nssi.linking.LinkingModuleConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Service(value = "geonamesService")
public class GeonamesService implements AsyncLinkingService {

    private final GeonamesRepository repository;

    @Autowired
    public GeonamesService(GeonamesRepository repository) {
        this.repository = repository;
    }

    @Override
    public LinkingServiceName getName() {
        return LinkingServiceName.GEONAMES;
    }

    @Async
    @Override
    public CompletableFuture<Optional<LinkingServiceResult>> getLinks(String entity, String type)
    {
        if (!type.equals(EntityClassification.LOCATION.name())) {
            return CompletableFuture.completedFuture(Optional.empty());
        }

        GeonamesResponse result = repository.getResult(entity);

        List<EntityLinkDetails> matches = Optional.ofNullable(result.getGeonames())
                .map(geonames -> geonames.stream()
                        .map(geoname -> EntityLinkDetails.builder()
                                .uri("http://geonames.org/" + geoname.getGeonameId())
                                .heading(geoname.getToponymName())
                                .description(geoname.getFcodeName())
                                .build()
                        ).collect(Collectors.toList()))
                .orElse(Collections.emptyList());

        return CompletableFuture.completedFuture(
                LinkingServiceResult.builder()
                        .serviceName(this.getName().getServiceName())
                        .serviceURI(this.getName().getUri())
                        .matches(matches)
                        .buildOptional()
        );
    }
}
