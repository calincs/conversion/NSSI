package ca.lincsproject.nssi.linking.repository;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * A class modelling the response from the DBPedia API.
 */

@Data
@NoArgsConstructor
@JacksonXmlRootElement(localName = "ArrayOfResult")
public class DBPediaResponse {
    @Data
    @NoArgsConstructor
    public static class DBPediaResult {

        @JacksonXmlProperty(localName = "Label")
        private String label;

        @JacksonXmlProperty(localName = "URI")
        private String uri;

        @JacksonXmlProperty(localName = "Description")
        private String description;
    }

    @JacksonXmlProperty(localName = "Result")
    private List<DBPediaResult> results;
}
