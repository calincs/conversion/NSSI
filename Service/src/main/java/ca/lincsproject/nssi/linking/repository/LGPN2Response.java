package ca.lincsproject.nssi.linking.repository;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * A class modelling the response from the LGPN2 API.
 */

@Data
@NoArgsConstructor
public class LGPN2Response {

    @Data
    @NoArgsConstructor
    public static class LGPN2PersonResponse {
        private String id;
        private String name;
        private String place;
        private String notBefore;
        private String notAfter;
    }

    private List<LGPN2PersonResponse> persons;
}
