package ca.lincsproject.nssi.linking;

public class LinkingModuleConstants {
    public static final String PROFILE_NAME = "linking";
    public static final String QUEUE_NAME = "linkingQueue";
    public static final String SERVICE_NAME = "Linking Service";
}
