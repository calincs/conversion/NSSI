package ca.lincsproject.nssi.linking.repository;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * A class modelling the VIAF record response.
 */

@Data
@NoArgsConstructor
public class VIAFRecordResponse implements Serializable {

    @Data
    @NoArgsConstructor
    public static class VIAFRecordDataResponse implements Serializable {
        private String viafID;

        @JsonProperty("Document")
        private Map<String, Object> document;

        private Map<String, Object> mainHeadings;
    }

    private String recordSchema;
    private VIAFRecordDataResponse recordData;

    public String getAboutURI() {
        return (String) recordData.document.getOrDefault("@about", null);
    }

    public String getMainHeading() {
        Object data = recordData.mainHeadings.getOrDefault("data", null);
        if (Objects.isNull(data)) {
            return null;
        }

        if (data instanceof Map) {
            return (String) ((Map) data).getOrDefault("text", null);
        }

        if (data instanceof List && !((List) data).isEmpty()) {
            return (String) ((Map) ((List) data).get(0)).getOrDefault("text", null);
        }

        return null;
    }
}
