package ca.lincsproject.nssi.linking.repository;

import ca.lincsproject.nssi.linking.LinkingModuleConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Repository
public class GettyRepository {

    @Autowired
    private RestTemplate restTemplate;
    private final String sparqlQuery =
            "select ?Subject ?Term ?Parents ?Descr ?ScopeNote ?Type (coalesce(?Type1,?Type2) as ?ExtraType) {" +
                    " ?Subject luc:term \"%s\"; a ?typ; skos:inScheme ulan:." +
                    " ?typ rdfs:subClassOf gvp:Subject; rdfs:label ?Type." +
                    " filter (?typ != gvp:Subject)" +
                    " optional {?Subject gvp:placeTypePreferred [gvp:prefLabelGVP [xl:literalForm ?Type1]]}" +
                    " optional {?Subject gvp:agentTypePreferred [gvp:prefLabelGVP [xl:literalForm ?Type2]]}" +
                    " optional {?Subject gvp:prefLabelGVP [xl:literalForm ?Term]}" +
                    " optional {?Subject gvp:parentStringAbbrev ?Parents}" +
                    " optional {?Subject foaf:focus/gvp:biographyPreferred/schema:description ?Descr}" +
                    " optional {?Subject skos:scopeNote [dct:language gvp_lang:en; rdf:value ?ScopeNote]}}" +
                    " LIMIT %d";
    private final String requestURIBase = "https://lookup.services.cwrc.ca/getty/sparql.json?query={query}";

    private final long maxResults;

    public GettyRepository(@Value("${nssi.linking.getty.maxResults:5}") long maxResults) {
        this.maxResults = maxResults;
    }

    public GettyResponse getResult(String entity) {
        String query = String.format(sparqlQuery, entity, maxResults);
        return restTemplate.getForObject(requestURIBase, GettyResponse.class, query);
    }
}
