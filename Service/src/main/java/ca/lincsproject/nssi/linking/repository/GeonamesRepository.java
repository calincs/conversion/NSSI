package ca.lincsproject.nssi.linking.repository;

import ca.lincsproject.nssi.linking.LinkingModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Repository
public class GeonamesRepository {
    private final RestTemplate restTemplate;

    private final String requestURIBase = "https://secure.geonames.org/searchJSON";

    private final long maxResults;

    private final Logger LOGGER = LoggerFactory.getLogger(GeonamesRepository.class);

    @Autowired
    public GeonamesRepository(@Value("${nssi.linking.geonames.maxResults:5}") long maxResults,
                              RestTemplate restTemplate)
    {
        this.maxResults = maxResults;
        this.restTemplate = restTemplate;
    }

    @Cacheable("geonamesEntity")
    public GeonamesResponse getResult(String entity) {
        return restTemplate.getForObject(getRequestURI(entity), GeonamesResponse.class);
    }

    private String getRequestURI(String entity) {
        return UriComponentsBuilder.fromUriString(requestURIBase)
                .queryParam("q", entity)
                .queryParam("username", "cwrcgeonames")
                .queryParam("maxRows", maxResults)
                .build()
                .toUriString();
    }
}
