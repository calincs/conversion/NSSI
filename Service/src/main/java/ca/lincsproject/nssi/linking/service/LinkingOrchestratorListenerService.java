package ca.lincsproject.nssi.linking.service;

import ca.lincsproject.nssi.broker.message.LinkedEntityInfo;
import ca.lincsproject.nssi.broker.message.NSSIServiceMessage;
import ca.lincsproject.nssi.broker.message.NamedEntityInfo;
import ca.lincsproject.nssi.broker.service.NSSIServiceUtils;
import ca.lincsproject.nssi.broker.service.NSSIServiceUtilsFactory;
import ca.lincsproject.nssi.linking.LinkingModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Service
public class LinkingOrchestratorListenerService {
    private final LinkingOrchestratorService linkingOrchestratorService;
    private final NSSIServiceUtils<NamedEntityInfo, LinkedEntityInfo> nssiServiceUtils;

    private final Logger LOGGER = LoggerFactory.getLogger(LinkingOrchestratorService.class);

    @Autowired
    LinkingOrchestratorListenerService(@Value("${nssi.processing.services.simple_linking.fullName}") String fullName,
                                       LinkingOrchestratorService linkingOrchestratorService,
                                       NSSIServiceUtilsFactory nssiServiceUtilsFactory)
    {
        this.nssiServiceUtils = nssiServiceUtilsFactory.nssiServiceUtils(
                NamedEntityInfo.class,
                LinkedEntityInfo.class,
                fullName
        );
        this.linkingOrchestratorService = linkingOrchestratorService;
    }

    @RabbitListener(queues = "linkingQueue")
    public void listen(Message message) {
        nssiServiceUtils.processWithRetry(message, this::processMessage);
    }

    private void processMessage(NSSIServiceMessage message, NamedEntityInfo neInfo) throws Throwable {
        LOGGER.info("Processing linking message for entity: " + neInfo.getEntity()
                + ", of type: " + neInfo.getClassification().name());

        LinkedEntityInfo leInfo =
                linkingOrchestratorService.getLinksForEntity(neInfo, message.getAuthorities());

        LOGGER.error(leInfo.toString());

        nssiServiceUtils.sendToNextStep(message, leInfo);
    }
}
