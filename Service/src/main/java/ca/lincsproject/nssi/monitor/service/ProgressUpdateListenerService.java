package ca.lincsproject.nssi.monitor.service;

import ca.lincsproject.nssi.broker.infrastructure.config.RMQConfiguration;
import ca.lincsproject.nssi.broker.message.ProgressMessage;
import ca.lincsproject.nssi.monitor.MonitorModuleConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Profile(MonitorModuleConstants.PROFILE_NAME)
@Service
public class ProgressUpdateListenerService {
    private final ProgressUpdateService progressUpdateService;
    private final ObjectMapper objectMapper;

    private final Logger LOGGER = LoggerFactory.getLogger(ProgressUpdateListenerService.class);

    @Autowired
    ProgressUpdateListenerService(ProgressUpdateService progressUpdateService,
                                  ObjectMapper objectMapper) {
        this.progressUpdateService = progressUpdateService;
        this.objectMapper = objectMapper;
    }

    @RabbitListener(queues = RMQConfiguration.PROGRESS_QUEUE)
    public void listen(Message message) throws IOException {
        ProgressMessage progressMessage = objectMapper.readValue(message.getBody(), ProgressMessage.class);
        MDC.put("jobId", String.valueOf(progressMessage.getJobId()));
        MDC.put("requestId", progressMessage.getRequestId());
        MDC.put("service", MonitorModuleConstants.SERVICE_NAME);

        LOGGER.debug("Received Progress Message");

        try {
            progressUpdateService.updateJobProgressAndStatus(progressMessage);
        } finally {
            MDC.clear();
        }
    }
}
