package ca.lincsproject.nssi.monitor.service;

import ca.lincsproject.nssi.broker.message.ResultMessage;
import ca.lincsproject.nssi.job.service.JobService;
import ca.lincsproject.nssi.monitor.MonitorModuleConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(MonitorModuleConstants.PROFILE_NAME)
@Service
@Transactional
public class ResultsUpdateService {
    private final JobService jobService;
    private final ObjectMapper objectMapper;

    private final Logger LOGGER = LoggerFactory.getLogger(ProgressUpdateService.class);

    @Autowired
    ResultsUpdateService(JobService jobService,
                         ObjectMapper objectMapper)
    {
        this.jobService = jobService;
        this.objectMapper = objectMapper;
    }

    public void updateJobResultsInfo(ResultMessage message) {
        try {
            String paramsJson = objectMapper.writeValueAsString(message.getResultStoreParams());
            jobService.updateResultsInfo(message.getJobId(), paramsJson);
        } catch (JsonProcessingException e) {
            LOGGER.error("FAILED To Process Results Message",  
                        kv("stackTrace", e.getStackTrace()), 
                        kv("service", MonitorModuleConstants.SERVICE_NAME),
                        kv("jobId", message.getJobId()),
                        kv("requestId", message.getRequestId()));
        }
    }
}
