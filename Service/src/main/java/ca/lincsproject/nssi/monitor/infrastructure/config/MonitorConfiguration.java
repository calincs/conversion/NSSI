package ca.lincsproject.nssi.monitor.infrastructure.config;

import ca.lincsproject.nssi.monitor.MonitorModuleConstants;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile(MonitorModuleConstants.PROFILE_NAME)
@Configuration
public class MonitorConfiguration {
}
