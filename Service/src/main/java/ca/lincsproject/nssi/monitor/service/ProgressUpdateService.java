package ca.lincsproject.nssi.monitor.service;

import ca.lincsproject.nssi.api.infrastructure.config.NSSIProcessingConfigProperties;
import ca.lincsproject.nssi.broker.message.ProgressMessage;
import ca.lincsproject.nssi.broker.service.RMQUtilsService;
import ca.lincsproject.nssi.job.model.JobDTO;
import ca.lincsproject.nssi.job.model.JobStatus;
import ca.lincsproject.nssi.job.model.TaskStatus;
import ca.lincsproject.nssi.job.service.JobService;
import ca.lincsproject.nssi.job.service.TaskUpdateService;
import ca.lincsproject.nssi.job.service.WorkflowETAService;
import ca.lincsproject.nssi.monitor.MonitorModuleConstants;
import ca.lincsproject.nssi.notification.service.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Profile(MonitorModuleConstants.PROFILE_NAME)
@Service
@Transactional
public class ProgressUpdateService {
    private final JobService jobService;
    private final TaskUpdateService taskUpdateService;
    private final WorkflowETAService workflowETAService;
    private final NSSIProcessingConfigProperties configProperties;
    private final RMQUtilsService rmqUtils;
    private final NotificationService notifyService;

    private final Logger LOGGER = LoggerFactory.getLogger(ProgressUpdateService.class);

    @Autowired
    ProgressUpdateService(JobService jobService,
                          TaskUpdateService taskUpdateService,
                          WorkflowETAService workflowETAService,
                          NSSIProcessingConfigProperties configProperties,
                          RMQUtilsService rmqUtils,
                          NotificationService notifyService)
    {
        this.jobService = jobService;
        this.taskUpdateService = taskUpdateService;
        this.workflowETAService = workflowETAService;
        this.configProperties = configProperties;
        this.rmqUtils = rmqUtils;
        this.notifyService = notifyService;
    }

    public void updateJobProgressAndStatus(ProgressMessage message) {
        LOGGER.info("Updating job progress");
        getTaskStatus(message).ifPresent(status ->
                taskUpdateService.updateTask(
                        message.getJobId(),
                        message.getTaskMessageId(),
                        message.getServiceName(),
                        status,
                        message.getDetails())
        );

        if (Objects.nonNull(message.getFailed()) && message.getFailed()) {
            LOGGER.info("Updating job failed status");
            jobService.updateFailedStatus(message.getJobId());
        }
    }

    @Scheduled(fixedDelayString = "${nssi.monitor.job.status.check_interval_in_ms}")
    public void checkForCompletedOrFailedJobs() {
        LOGGER.trace("checking for completed jobs");
        List<JobDTO> uncompletedJobs = jobService.getUncompletedJobs().stream()
                .filter(jobDTO -> jobDTO.getStatus() == JobStatus.IN_PROGRESS)
                .collect(Collectors.toList());

        LOGGER.trace("there are " + uncompletedJobs.size() + " uncompleted jobs");
        uncompletedJobs.forEach(job -> {
            Long jobId = job.getId();
            int queued = taskUpdateService.getUpdates(jobId, TaskStatus.QUEUED).size();
            int completed = taskUpdateService.getUpdates(jobId, TaskStatus.COMPLETED).size();

            LOGGER.trace("job: " + jobId + "; q'd: " + queued + "; cmp: " + completed);

            if (queued == completed) {
                jobService.updateCompletedStatus(jobId);
                LOGGER.info("Job Completed");
                notifyService.sendFinishedMessage(jobId);
            }

            int failed = taskUpdateService.getUpdates(jobId, TaskStatus.FAILED).size();
            if (failed > 0) {
                LOGGER.trace("job: " + jobId + "; failed count: " + failed);
                jobService.updateFailedStatus(jobId);
                LOGGER.info("Job completed with failures");
                notifyService.sendFinishedMessage(jobId);
            }
        });
    }

    @Scheduled(fixedDelayString = "${nssi.monitor.job.eta_update.check_interval_in_ms}")
    public void updateWorkflowETA() {
        for (String workflow : configProperties.getWorkflows().keySet()) {
            List<NSSIProcessingConfigProperties.ServiceConfig> services = configProperties.getServiceConfigs(workflow);
            Double estimated = services.stream()
                    .map(this::calculateETA)
                    .reduce(0.0, Double::sum);

            workflowETAService.updateWorkflowETA(workflow, estimated);
        }
    }

    @Scheduled(cron = "${nssi.monitor.job.task_update_cleanup_cron}")
    public void cleanUpTaskUpdates() {
        taskUpdateService.findDistinctJobIds().forEach(jobId -> {
            jobService.getJob(jobId).ifPresent(jobDTO -> {
                if (jobDTO.getStatus() == JobStatus.READY) {
                    taskUpdateService.deleteTaskUpdatesForJob(jobId);
                }
            });
        });
    }

    private Double calculateETA(NSSIProcessingConfigProperties.ServiceConfig service) {
        Integer recentCompleted = taskUpdateService.getRecentUpdateCounts(service.getFullName(), TaskStatus.COMPLETED);
        Integer totalQueued = taskUpdateService.getUpdateCounts(service.getFullName(), TaskStatus.QUEUED);

        if (recentCompleted == 0) {
            return 0.0;
        }

        Double rate = 30.0 / Double.valueOf(recentCompleted);
        return rate * totalQueued;
    }

    private Optional<TaskStatus> getTaskStatus(ProgressMessage message) {
        if (Objects.nonNull(message.getQueued())
                && message.getQueued()) return Optional.of(TaskStatus.QUEUED);

        if (Objects.nonNull(message.getInProgress())
                && message.getInProgress()) return Optional.of(TaskStatus.IN_PROGRESS);

        if (Objects.nonNull(message.getCancelled())
                && message.getCancelled()) return Optional.of(TaskStatus.CANCELLED);

        if (Objects.nonNull(message.getFailed())
                && message.getFailed()) return Optional.of(TaskStatus.FAILED);

        if (Objects.nonNull(message.getCompleted())
                && message.getCompleted()) return Optional.of(TaskStatus.COMPLETED);

        return Optional.empty();
    }
}
