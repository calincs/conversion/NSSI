package ca.lincsproject.nssi.apiexception.infrastructure.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class ServiceExceptionHandler extends ResponseEntityExceptionHandler {
    private final Logger LOGGER = LoggerFactory.getLogger(ServiceExceptionHandler.class);

    @ExceptionHandler(DeleteIncompleteJobException.class)
    public final ResponseEntity<CustomExceptionSchema> handleDeleteException(DeleteIncompleteJobException e) {
        var error = CustomExceptionSchema.builder()
                .message(String.format("Job ID %d could not be deleted.", e.getJobId()))
                .details(String.format("The specified job may still be in progress. To send a " +
                        "cancel request instead, use `PUT /api/jobs/%d/actions/cancel`", e.getJobId()))
                .build();
        return new ResponseEntity<>(error, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(JobNotReadyException.class)
    public final ResponseEntity<CustomExceptionSchema> handlePrematureResultsException(JobNotReadyException e) {
        var error = CustomExceptionSchema.builder()
                .message(String.format("Job ID %d has not completed processing, so no results " +
                        "are yet available.", e.getJobId()))
                .build();
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(JobCancelledOrFailedException.class)
    public final ResponseEntity<CustomExceptionSchema> handleJobCancelledOrFailedException(JobCancelledOrFailedException e) {
        var error = CustomExceptionSchema.builder()
                .message(String.format("Job ID %d has cancelled or failed status.", e.getJobId()))
                .build();
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CancelCompletedJobException.class)
    public final ResponseEntity<CustomExceptionSchema> handleCancelException(CancelCompletedJobException e) {
        var error = CustomExceptionSchema.builder()
                .message(String.format("Job ID %d could not be cancelled.", e.getJobId()))
                .details(String.format("The specified job has already completed. To send a " +
                        "delete request instead, use `DELETE /api/jobs/%d`", e.getJobId()))
                .build();
        return new ResponseEntity<>(error, HttpStatus.CONFLICT);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request)
    {
        var error = CustomExceptionSchema.builder()
                .message("The request body could not be parsed correctly.")
                .details(e.getMessage())
                .build();
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @Override
    @Nullable
    protected ResponseEntity<Object> handleAsyncRequestTimeoutException(AsyncRequestTimeoutException e,
                                                                        HttpHeaders headers,
                                                                        HttpStatus status,
                                                                        WebRequest webRequest)
    {
        if (webRequest instanceof ServletWebRequest) {
            ServletWebRequest servletWebRequest = (ServletWebRequest) webRequest;
            HttpServletResponse response = servletWebRequest.getResponse();
            if (response != null && response.isCommitted()) {
                return null;
            }
        }

        return super.handleExceptionInternal(e, null, headers, status, webRequest);
    }

}
