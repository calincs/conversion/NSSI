package ca.lincsproject.nssi.apiexception.infrastructure.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JobCancelledOrFailedException extends RuntimeException {
    private Long jobId;
}
