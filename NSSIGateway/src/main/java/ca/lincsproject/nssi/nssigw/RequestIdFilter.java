package ca.lincsproject.nssi.nssigw;

import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class RequestIdFilter implements GlobalFilter {
    private Long requestId = 0L;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        requestId += 1L;
        return chain.filter(
            exchange.mutate().request(
                exchange.getRequest().mutate()
                    .header("requestId", requestId.toString())
                    .build()
            ).build()
        );
    }
}