package ca.lincsproject.nssi.nssigw;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfiguration {

    @Bean
    public LincsRoleRoutePredicateFactory lincsRole() {
        return new LincsRoleRoutePredicateFactory();
    }
}
