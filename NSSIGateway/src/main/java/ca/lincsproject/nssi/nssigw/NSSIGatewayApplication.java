package ca.lincsproject.nssi.nssigw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
public class NSSIGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(NSSIGatewayApplication.class, args);
    }

    @Profile("docker")
    @Bean
    public RouteLocator routeLocatorDocker(RouteLocatorBuilder builder,
                                           LincsRoleRoutePredicateFactory lr)
    {
        return builder.routes()
                .route("default", r -> r.path("/**")
                        .uri("http://nssi:8080"))
                .build();
    }

    @Profile({"sandbox", "stage", "prod"})
    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder,
                                     LincsRoleRoutePredicateFactory lr)
    {
        return builder.routes()
                .route("nssi", r -> r.path("/**").uri("http://api-default:8080"))
                .build();
    }
}
