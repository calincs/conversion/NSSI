package ca.lincsproject.nssi.nssigw;

import org.keycloak.TokenVerifier;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.AccessToken;
import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.web.server.ServerWebExchange;

import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;

public class LincsRoleRoutePredicateFactory extends
        AbstractRoutePredicateFactory<LincsRoleRoutePredicateFactory.Config>
{

    public LincsRoleRoutePredicateFactory() {
        super(Config.class);
    }

    @Override
    public Predicate<ServerWebExchange> apply(Config config) {
        return (ServerWebExchange t) -> {
            try {
                String bearerToken = Objects.requireNonNull(t.getRequest().getHeaders().get("Authorization"))
                        .get(0)
                        .substring(7); // remove "Bearer " prefix

                AccessToken token = TokenVerifier.create(bearerToken, AccessToken.class).getToken();
                Set<String> roles = token.getRealmAccess().getRoles();

                return roles.contains(config.roleToRoute);
            } catch (VerificationException e) {
                e.printStackTrace();
            }

            return false;
        };
    }

    public static class Config {
        private final String roleToRoute;

        Config(String roleToRoute) {
            this.roleToRoute = roleToRoute;
        }
    }
}
