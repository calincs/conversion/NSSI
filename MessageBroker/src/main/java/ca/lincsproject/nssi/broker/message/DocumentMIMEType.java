package ca.lincsproject.nssi.broker.message;

import com.fasterxml.jackson.annotation.JsonValue;

public enum DocumentMIMEType {
    TEI_XML("application/tei+xml"),
    ORLANDO_XML("application/orlando+xml"), // TODO: check
    MODS("application/mods+xml"),
    EAD("application/ead+xml"), // TODO: check
    PDF("application/pdf"),
    JSON("application/json"),
    HTML("text/html"),
    PLAIN_TEXT("text/plain");

    private String mediaTypeName;

    DocumentMIMEType(String name) {
        this.mediaTypeName = name;
    }

    @JsonValue
    public String getMediaTypeName() {
        return this.mediaTypeName;
    }

    public void setMediaTypeName(String name) {
        this.mediaTypeName = name;
    }
}
