package ca.lincsproject.nssi.broker.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResultMessage {
    /**
     * The job ID.
     */
    private Long jobId;

    /**
     * The request ID.
     */
    private String requestId;

    /**
     * A map containing parameters that specify where the results have been
     * saved on S3.
     */
    private Map<String, String> resultStoreParams;
}
