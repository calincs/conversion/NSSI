package ca.lincsproject.nssi.broker.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

@Data
@Builder(builderClassName = "Builder")
@AllArgsConstructor
@NoArgsConstructor
public class ProgressMessage {
    /**
     * The job ID.
     */
    private Long jobId;

    /**
     * The request ID.
     */
    private String requestId;

    /**
     * The message ID for the task whose progress is being reported.
     */
    private String taskMessageId;

    /**
     * The name of the service that sent this progress message.
     */
    private String serviceName;

    /**
     * The number of new tasks started for the given job.
     */
    @Deprecated
    private Integer numStarted;

    /**
     * The number of new tasks completed for the given job.
     */
    @Deprecated
    private Integer numCompleted;

    /**
     * The number of new transient failures for the given job.
     */
    @Deprecated
    private Integer numTransientFailures;

    /**
     * The number of new permanent failures for the given job.
     */
    @Deprecated
    private Integer numPermanentFailures;

    /**
     * Whether the task has been queued.
     */
    private Boolean queued;

    /**
     * Whether the task is in progress.
     */
    private Boolean inProgress;

    /**
     * Whether the task has been cancelled.
     */
    private Boolean cancelled;

    /**
     * Whether the task has failed.
     */
    private Boolean failed;

    /**
     * Whether the task has been completed.
     */
    private Boolean completed;

    /**
     * Details about the update.
     */
    @Nullable
    private String details;
}
