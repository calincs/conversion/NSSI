package ca.lincsproject.nssi.broker.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CancelMessage implements ServiceMessage {
    /**
     * The job ID.
     */
    private Long jobId;

    /**
     * The request ID.
     */
    private String requestId;

    /**
     * The cancellation pipeline.
     */
    private List<String> pipeline;
}
