package ca.lincsproject.nssi.broker.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NSSIServiceUtilsFactory {
    private Integer maxRetries;
    private RMQUtilsService rmqUtils;
    private ObjectMapper objectMapper;

    @Autowired
    public NSSIServiceUtilsFactory(@Value("${nssi.rmq.max-retries:0}") int maxRetries,
                                   RMQUtilsService rmqUtils,
                                   ObjectMapper objectMapper)
    {
        this.maxRetries = maxRetries;
        this.rmqUtils = rmqUtils;
        this.objectMapper = objectMapper;
    }

    public <T, U> NSSIServiceUtils<T, U> nssiServiceUtils(Class<T> inputType,
                                                    Class<U> outputType,
                                                    String serviceName)
    {
        return NSSIServiceUtils.<T, U>builder()
                .serviceName(serviceName)
                .maxRetries(maxRetries)
                .inputType(inputType)
                .outputType(outputType)
                .rmqUtils(rmqUtils)
                .objectMapper(objectMapper)
                .build();
    }
}
