package ca.lincsproject.nssi.broker.message;

import java.util.List;

public interface ServiceMessage {
    Long getJobId();
    String getRequestId();
    List<String> getPipeline();
    void setJobId(Long jobId);
    void setPipeline(List<String> pipeline);
}