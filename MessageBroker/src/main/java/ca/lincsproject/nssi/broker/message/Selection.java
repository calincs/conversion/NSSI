package ca.lincsproject.nssi.broker.message;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

/**
 * A class that represents a selection within a text.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Selection {
    /**
     * The xpath containing the selection. If this value is null, then the `start` and `end`
     * offsets are absolute, from the beginning of the document.
     */
    @Nullable
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String xpath;

    /**
     * The start character position of the selection, within the node identified by `xpath`.
     */
    private Integer start;

    /**
     * The end character position of the selection, within the node identified by `xpath`.
     */
    private Integer end;
}
