package ca.lincsproject.nssi.broker.infrastructure.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RMQConfiguration {
    public static final String PROGRESS_QUEUE = "progressQueue";
    public static final String RESULT_QUEUE = "resultQueue";
    public static final String FIRST_RETRY_QUEUE = "nssiFirstRetryQueue";
    public static final String SECOND_RETRY_QUEUE = "nssiSecondRetryQueue";
    public static final String PARKING_LOT_QUEUE = "parkingLotQueue";
    public static final String APP_MESSAGES_EXCHANGE = "appMessagesExchange";
    public static final String PROGRESS_EXCHANGE = "progressExchange"; // rename this to monitor exchange?
    public static final String DLQ = "deadLetterQueue";
    public static final String DLQ_KEY = "x-dead-letter-exchange";
    public static final String DLX_APP_MESSAGES = "appMessages.dlx";
    public static final String TTL_KEY = "x-message-ttl";
    public static final String[] RETRY_QUEUES = {FIRST_RETRY_QUEUE, SECOND_RETRY_QUEUE};

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final var rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public RabbitAdmin rabbitAdmin(final ConnectionFactory connectionFactory) {
        return new RabbitAdmin(connectionFactory);
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public Queue progressQueue() {
        return QueueBuilder.durable(PROGRESS_QUEUE)
                .withArgument(DLQ_KEY, DLX_APP_MESSAGES)
                .build();
    }

    @Bean
    public Queue resultQueue() {
        return QueueBuilder.durable(RESULT_QUEUE)
                .withArgument(DLQ_KEY, DLX_APP_MESSAGES)
                .build();
    }

    @Bean
    public Queue nssiFirstRetryQueue() {
        return QueueBuilder.durable(FIRST_RETRY_QUEUE)
                .withArgument(DLQ_KEY, DLX_APP_MESSAGES)
                .withArgument(TTL_KEY, 60000)
                .build();
    }

    @Bean
    public Queue nssiSecondRetryQueue() {
        return QueueBuilder.durable(SECOND_RETRY_QUEUE)
                .withArgument(DLQ_KEY, DLX_APP_MESSAGES)
                .withArgument(TTL_KEY, 300000)
                .build();
    }

    @Bean
    public Queue parkingLotQueue() {
        return QueueBuilder.durable(PARKING_LOT_QUEUE)
                .withArgument(DLQ_KEY, DLX_APP_MESSAGES)
                .build();
    }

    @Bean
    public Queue deadLetterQueue() {
        return QueueBuilder.durable(DLQ).build();
    }

    @Bean(name = APP_MESSAGES_EXCHANGE)
    public DirectExchange appMessagesExchange() {
        return new DirectExchange(APP_MESSAGES_EXCHANGE);
    }

    @Bean
    public DirectExchange progressExchange() {
        return new DirectExchange(PROGRESS_EXCHANGE);
    }

    @Bean
    public FanoutExchange deadLetterExchange() {
        return new FanoutExchange(DLX_APP_MESSAGES);
    }

    @Bean
    public Binding progressBinding() {
        return BindingBuilder.bind(progressQueue()).to(progressExchange())
                .with(PROGRESS_QUEUE);
    }

    @Bean
    public Binding resultBinding() {
        return BindingBuilder.bind(resultQueue()).to(progressExchange())
                .with(RESULT_QUEUE);
    }

    @Bean
    public Binding deadLetterBinding() {
        return BindingBuilder.bind(deadLetterQueue()).to(deadLetterExchange());
    }
}