package ca.lincsproject.nssi.broker.service;

public interface ThrowingStringConsumer<T, E extends Throwable> extends ThrowingBiConsumer<T, String, E> {
}
