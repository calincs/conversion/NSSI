package ca.lincsproject.nssi.broker.service;

import ca.lincsproject.nssi.broker.MessageBrokerModuleConstants;
import ca.lincsproject.nssi.broker.infrastructure.config.RMQConfiguration;
import ca.lincsproject.nssi.broker.message.NSSIServiceMessage;
import ca.lincsproject.nssi.broker.message.ProgressMessage;
import ca.lincsproject.nssi.broker.message.ResultMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static ca.lincsproject.nssi.broker.infrastructure.config.RMQConfiguration.APP_MESSAGES_EXCHANGE;
import static net.logstash.logback.argument.StructuredArguments.kv;

@Builder
@Data
@AllArgsConstructor
public class NSSIServiceUtils<T, U> {
  private final String serviceName;
  private final Integer maxRetries;
  private final Class<T> inputType;
  private final Class<U> outputType;
  private final ObjectMapper objectMapper;
  private final RMQUtilsService rmqUtils;

  private final Logger LOGGER = LoggerFactory.getLogger(NSSIServiceUtils.class);

  public void processWithRetry(Message message,
      ThrowingVoidConsumer<NSSIServiceMessage, Throwable> messageConsumer) {
    parseMessage(message).ifPresentOrElse(serviceMessage -> {
      try {
        initializeLoggerContext(serviceMessage);
        sendProgressMessage(buildProgressMessage(serviceMessage).inProgress(true).build());
        messageConsumer.accept(serviceMessage, null);
        sendProgressMessage(buildProgressMessage(serviceMessage).completed(true).build());
      } catch (Throwable t) {
        handleServiceFailure(message, serviceMessage, t);
      } finally {
        clearLoggerContext();
      }
    },
        () -> {
          LOGGER.error("Could not parse NSSI message: " + message);
          sendToParkingLot(message);
        });
  }

  public void processWithRetry(Message message,
      ThrowingStringConsumer<NSSIServiceMessage, Throwable> messageConsumer) {
    parseMessage(message).ifPresentOrElse(serviceMessage -> {
      try {
        initializeLoggerContext(serviceMessage);
        sendProgressMessage(buildProgressMessage(serviceMessage).inProgress(true).build());
        messageConsumer.accept(serviceMessage, serviceMessage.getDocument());
        sendProgressMessage(buildProgressMessage(serviceMessage).completed(true).build());
      } catch (Throwable t) {
        handleServiceFailure(message, serviceMessage, t);
      } finally {
        clearLoggerContext();
      }
    },
        () -> {
          LOGGER.error("Could not parse NSSI message: " + message);
          sendToParkingLot(message);
        });
  }

  public void processWithRetry(Message message,
      ThrowingBiConsumer<NSSIServiceMessage, T, Throwable> messageConsumer) {
    parseMessage(message).ifPresentOrElse(serviceMessage -> {
      try {
        initializeLoggerContext(serviceMessage);
        sendProgressMessage(buildProgressMessage(serviceMessage).inProgress(true).build());
        T inputDocument = null;
        if (Objects.nonNull(serviceMessage.getDocument())) {
          inputDocument = objectMapper.readValue(serviceMessage.getDocument(), inputType);
        }
        messageConsumer.accept(serviceMessage, inputDocument);
        sendProgressMessage(buildProgressMessage(serviceMessage).completed(true).build());
      } catch (Throwable t) {
        handleServiceFailure(message, serviceMessage, t);
      } finally {
        clearLoggerContext();
      }
    },
        () -> {
          LOGGER.error("Could not parse NSSI message: " + message);
          sendToParkingLot(message);
        });
  }

  public void sendToNextStep(NSSIServiceMessage originalMessage, U result) {
    List<String> pipeline = originalMessage.getPipeline();
    NSSIServiceMessage message = originalMessage.toBuilder().build();

    if (Objects.isNull(pipeline) || pipeline.isEmpty()) {
      sendProgressMessage(buildProgressMessage(originalMessage).completed(true).build());
    } else {
          try {
              if (result instanceof String) {
                  message.setDocument((String) result);
              } else {
                  String outputDocument = objectMapper.writeValueAsString(result);
                  message.setDocument(outputDocument);
              }
      } catch (JsonProcessingException e) {
        LOGGER.error("Could not serialize result: " + e);
        sendProgressMessage(buildProgressMessage(originalMessage).failed(true).build());
      }

      String next = pipeline.get(0);
      List<String> remainingAfterNext = pipeline.subList(1, pipeline.size());
      message.setPipeline(remainingAfterNext);
      message.setMessageId(UUID.randomUUID().toString());

      sendProgressMessage(buildProgressMessage(originalMessage).queued(true).build());
      rmqUtils.sendToQueue(APP_MESSAGES_EXCHANGE, next, message);
    }
  }

  public void sendResultMessage(ResultMessage message) {
    rmqUtils.sendToQueue(RMQConfiguration.PROGRESS_EXCHANGE, RMQConfiguration.RESULT_QUEUE, message);
  }

  private Optional<NSSIServiceMessage> parseMessage(Message message) {
    try {
      return Optional.ofNullable(objectMapper.readValue(message.getBody(), NSSIServiceMessage.class));
    } catch (Exception e) {
      return Optional.empty();
    }
  }

  private void initializeLoggerContext(NSSIServiceMessage serviceMessage) {
    MDC.put("jobId", String.valueOf(serviceMessage.getJobId()));
    MDC.put("requestId", serviceMessage.getRequestId());
    MDC.put("service", MessageBrokerModuleConstants.SERVICE_NAME);
  }

  private void clearLoggerContext() {
    MDC.clear();
  }

  private void handleServiceFailure(Message message,
      NSSIServiceMessage serviceMessage,
      Throwable t) {

    LOGGER.error("FAILED To Process Message",
        kv("exception", t.getMessage()),
        kv("stackTrace", t.getStackTrace()));

    // If the message can still be retried, send it to a retry queue. Otherwise,
    // send it to the parking lot queue as a permanent failure.
    MessageProperties props = message.getMessageProperties();
    int retriedCount = Optional
        .ofNullable(props.<Integer>getHeader("x-retried-count"))
        .orElse(0);

    if (retriedCount < maxRetries) {
      String targetRetryQueue = RMQConfiguration.RETRY_QUEUES[retriedCount];
      props.setHeader("x-retried-count", retriedCount + 1);
      props.setHeader("x-original-exchange", props.getReceivedExchange());
      props.setHeader("x-original-routing-key", props.getReceivedRoutingKey());

      LOGGER.info("Retrying Failed Message",
          kv("targetQueue", targetRetryQueue),
          kv("failureLocation", message.getMessageProperties().getConsumerQueue()));

      rmqUtils.sendToQueue(targetRetryQueue, message);
    } else {
      sendToParkingLot(message);
      sendProgressMessage(buildProgressMessage(serviceMessage).failed(true).build());
    }
  }

  private ProgressMessage.Builder buildProgressMessage(NSSIServiceMessage message) {
    return ProgressMessage.builder()
        .jobId(message.getJobId())
        .requestId(message.getRequestId())
        .taskMessageId(message.getMessageId())
        .serviceName(serviceName);
  }

  private void sendProgressMessage(ProgressMessage message) {
    rmqUtils.sendToQueue(RMQConfiguration.PROGRESS_EXCHANGE, RMQConfiguration.PROGRESS_QUEUE, message);
  }

  private void sendToParkingLot(Message message) {
    LOGGER.info("Sending Failed Message To Parking Lot");
    rmqUtils.sendToQueue(RMQConfiguration.PARKING_LOT_QUEUE, message);
  }
}
