package ca.lincsproject.nssi.broker.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A data class that contains details for each URI match.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EntityLinkDetails {
    /**
     * A URI for the entity.
     */
    private String uri;

    /**
     * The heading given to the entity in an authority file.
     */
    private String heading;

    /**
     * The description found for the entity in an authority file.
     */
    private String description;
}
