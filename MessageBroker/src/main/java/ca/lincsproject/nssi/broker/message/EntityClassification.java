package ca.lincsproject.nssi.broker.message;

public enum EntityClassification {
    PERSON,
    ORGANIZATION,
    LOCATION,
    TITLE, // is this correct? is it person titles, like "king"?
    MISC
}
