package ca.lincsproject.nssi.broker.message;

import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LinkedEntityInfo {
    /**
     * Info for the named entity.
     */
    private NamedEntityInfo namedEntityInfo;

    /**
     * The links for the entity.
     */
    @Singular
    private List<LinkingServiceResult> links;
}
