# NSSI - NERVE Secure Scalable Infrastructure

NSSI is a platform for running data conversion workflows for generating linked data from structured and semi-structured data. Clients submit jobs
to NSSI specifying a predefined workflow, and receive a URI from where they can retrieve results when the processing completes.

## Building and running NSSI

These instructions cover how to build and run the application locally from source.

Prerequisites:

* OpenJDK 18
* Maven 3
* Docker Compose

### Installing OpenJDK 18

Download the OpenJDK 18.0.2 .tar.gz build from [here](https://jdk.java.net/18/) and copy it to your working folder.

```bash
tar -xvf openjdk-18._linux-{version}
sudo mv jdk-18* /opt/
export JAVA_HOME=/opt/jdk-18 export PATH=$PATH:$JAVA_HOME/bin
java --version
```

To uninstall, run `sudo rm -rf /opt/jdk-18 && export JAVA_HOME=`

### Importing the project

To set up the project in IntelliJ or your IDE of choice, import the pom.xml under the repository root (not the pom files under any of the subdirectories).

### Building the project

#### Checkout the repository

```bash
git clone git@gitlab.com:calincs/conversion/NSSI.git
or
git clone https://gitlab.com/calincs/conversion/NSSI.git
```

#### Build a local Docker image

There is a Maven wrapper included in the project which can be used to invoke build and test actions. To build local Docker images for NSSI and the Gateway, run the following from the repository root:

```bash
./mvnw clean install && ./mvnw jib:dockerBuild -pl Service/ -Ddebug
./mvnw clean install -pl NSSIGateway && ./mvnw compile jib:dockerBuild -pl NSSIGateway
```

You should see a newly created Docker images, e.g.:

```bash
$ docker images
REPOSITORY                                                  TAG            IMAGE ID       CREATED              SIZE
registry.gitlab.com/calincs/conversion/nssi/nssigw          latest         32753a9a9773   6 seconds ago        258MB
registry.gitlab.com/calincs/conversion/nssi/v2              latest-dev     3af2bacbc959   About a minute ago   881MB
```

### Running with Docker Compose

There are several Docker Compose files available in the repository to run NSSI in different configurations.
See the [NSSI wiki](https://gitlab.com/calincs/conversion/NSSI/-/wikis/Usage/Running-NSSI-Locally-with-Docker-Compose) for more details.
