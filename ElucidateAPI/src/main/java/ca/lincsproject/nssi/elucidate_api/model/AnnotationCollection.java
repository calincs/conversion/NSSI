package ca.lincsproject.nssi.elucidate_api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnnotationCollection {
    @JsonProperty("@context")
    private AnnotationContext jsonContext;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("@id")
    private String id;

    @JsonProperty("@type")
    private String type;

    private String label;
    private Integer total;
    private AnnotationPage first;
    private String last;
}
