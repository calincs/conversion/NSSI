package ca.lincsproject.nssi.elucidate_api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnnotationChoice {

    @JsonProperty("@type")
    private String type;

    @JsonProperty("as:items")
    private List<AnnotationBody> items;
}
