package ca.lincsproject.nssi.elucidate_api.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContainerResponse {

    @JsonProperty("@context")
    private List<String> jsonContext;

    private String id;
    private String type;
    private String label;
    private ContainerPageInfo first;
    private String last;
    private Integer total;
}
