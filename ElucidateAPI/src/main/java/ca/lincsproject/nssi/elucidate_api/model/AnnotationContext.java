package ca.lincsproject.nssi.elucidate_api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnnotationContext {

    @JsonProperty("dcterms:created")
    private Map<String, String> dctermsCreatedContext;

    @JsonProperty("dcterms:issued")
    private Map<String, String>  dctermsIssuedContext;

    @JsonProperty("oa:motivatedBy")
    private Map<String, String> oaMotivatedByContext;

    @JsonProperty("@language")
    private String language;

    @JsonProperty("rdf")
    private String rdfContext;

    @JsonProperty("rdfs")
    private String rdfSchemaContext;

    @JsonProperty("as")
    private String activityStreamsContext;

    @JsonProperty("cwrc")
    private String cwrcContext;

    @JsonProperty("bf")
    private String bibframeContext;

    @JsonProperty("dc")
    private String dcContext;

    @JsonProperty("dcterms")
    private String dctermsContext;

    @JsonProperty("oa")
    private String oaContext;

    @JsonProperty("schema")
    private String schemaContext;

    @JsonProperty("xsd")
    private String xsdContext;
}
