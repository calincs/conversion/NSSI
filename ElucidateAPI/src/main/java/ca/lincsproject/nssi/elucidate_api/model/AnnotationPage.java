package ca.lincsproject.nssi.elucidate_api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnnotationPage {
    @JsonProperty("@context")
    private AnnotationContext jsonContext;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("@id")
    private String id;

    @JsonProperty("@type")
    private String type;

    private AnnotationCollection partOf;
    private List<Annotation> items;
    private String next;
    private String prev;
    private Integer startIndex;
}
