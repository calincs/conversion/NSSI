package ca.lincsproject.nssi.elucidate_api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(builderClassName = "Builder")
@NoArgsConstructor
@AllArgsConstructor
public class TextPositionSelector {

    @JsonProperty("@type")
    private String type;

    @JsonProperty("oa:start")
    private Integer start;

    @JsonProperty("oa:end")
    private Integer end;
}
