package ca.lincsproject.nssi.job.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "projects")
public class Project {
    /**
     * The project ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The project name.
     */
    private String projectName;

    /**
     * The list of resources that comprise this project.
     */
    @OneToMany(mappedBy = "project")
    private List<ProjectResource> resources;

    /**
     * Whether the project is deleted (i.e., not visible)
     */
    private Boolean deleted;
}
