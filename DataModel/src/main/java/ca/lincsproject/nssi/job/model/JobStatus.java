package ca.lincsproject.nssi.job.model;

public enum JobStatus {
    IN_PROGRESS,
    READY,
    CANCELLED,
    FAILED
}
