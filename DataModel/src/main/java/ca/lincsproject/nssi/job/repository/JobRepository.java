package ca.lincsproject.nssi.job.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobRepository extends JpaRepository<Job, Long> {
    List<Job> findByResultsUri(String resultsUri);
    List<Job> findByAllCompletedTrue();
    List<Job> findByAllCompletedFalse();

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update #{#entityName} j set j.allCompleted = true, j.lastUpdatedTime = NOW() where j.id = :id")
    void markCompleted(@Param("id") Long id);
    
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update #{#entityName} j set j.cancelled = true, j.lastUpdatedTime = NOW() where j.id = :id")
    void markCancelled(@Param("id") Long id);

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("update #{#entityName} j set j.failed = true, j.lastUpdatedTime = NOW() where j.id = :id")
    void markFailed(@Param("id") Long id);
}
