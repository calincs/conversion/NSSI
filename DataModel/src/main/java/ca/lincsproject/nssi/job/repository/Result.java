package ca.lincsproject.nssi.job.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "results")
public class Result {
    /**
     * The result ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The job to which this result belongs.
     */
    @ManyToOne
    @JoinColumn(name = "job_id", nullable = false)
    private Job job;

    /**
     * Whether the results need to be downloaded from S3.
     */
    @Column(name = "is_on_s3")
    private boolean isOnS3;

    /**
     * A JSON string containing a map of parameters that describe how to access results
     * from S3 (if applicable).
     */
    @Column(columnDefinition = "TEXT")
    private String resultStoreParams;

    /**
     * A JSON document containing the result data.
     */
    private String document;
}
