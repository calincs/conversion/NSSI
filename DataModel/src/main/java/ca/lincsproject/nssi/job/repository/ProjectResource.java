package ca.lincsproject.nssi.job.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "projectresources")
public class ProjectResource {
    /**
     * The resource ID.
     */
    @Id
    private UUID id;

    /**
     * The project to which this resource belongs.
     */
    @ManyToOne
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    /**
     * The source document URI.
     */
    private String sourceDocumentUri;

    /**
     * The results URI.
     */
    private String resultsUri;

    /**
     * Whether the resource is deleted (i.e., not visible).
     */
    private Boolean deleted;
}
