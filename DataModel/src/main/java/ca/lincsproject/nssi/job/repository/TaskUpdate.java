package ca.lincsproject.nssi.job.repository;

import ca.lincsproject.nssi.job.model.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "taskUpdates")
public class TaskUpdate {
    /**
     * The update message ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The timestamp of the update.
     */
    private LocalDateTime updateTime;

    /**
     * The current status of the task.
     */
    @Enumerated(EnumType.ORDINAL)
    private TaskStatus status;

    /**
     * The message ID of the task
     */
    private String messageId;

    /**
     * The service which published the update.
     */
    private String serviceName;

    /**
     * Optional details about the update.
     */
    private String details;

    /**
     * The job to which this update message applies.
     */
    private Long jobId;
}
