package ca.lincsproject.nssi.job.model;

public enum TaskStatus {
    QUEUED,
    IN_PROGRESS,
    FAILED,
    CANCELLED,
    COMPLETED
}
