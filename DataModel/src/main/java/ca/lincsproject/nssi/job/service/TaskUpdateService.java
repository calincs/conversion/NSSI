package ca.lincsproject.nssi.job.service;

import ca.lincsproject.nssi.job.repository.TaskUpdate;
import ca.lincsproject.nssi.job.repository.TaskUpdateRepository;
import ca.lincsproject.nssi.job.model.TaskStatus;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class TaskUpdateService {
    private final TaskUpdateRepository taskUpdateRepository;

    TaskUpdateService(TaskUpdateRepository taskUpdateRepository) {
        this.taskUpdateRepository = taskUpdateRepository;
    }

    public void updateTask(Long jobId,
                          String messageId,
                          String serviceName,
                          TaskStatus status,
                          @Nullable String details) {

        TaskUpdate taskUpdate = TaskUpdate.builder()
                .jobId(jobId)
                .messageId(messageId)
                .serviceName(serviceName)
                .updateTime(LocalDateTime.now())
                .status(status)
                .details(Optional.ofNullable(details).orElse(""))
                .build();

        taskUpdateRepository.save(taskUpdate);
    }

    public List<TaskUpdate> getUpdates(Long jobId, TaskStatus status) {
        return taskUpdateRepository.findByJobIdAndStatus(jobId, status);
    }

    public Integer getRecentUpdateCounts(String serviceName, TaskStatus status) {
        LocalDateTime end = LocalDateTime.now();
        LocalDateTime start = end.minusMinutes(30); // TODO, configure interval
        return taskUpdateRepository.countTaskUpdateByServiceNameEqualsAndStatusEqualsAndUpdateTimeBetween(
                serviceName,
                status,
                start,
                end
        );
    }

    public Integer getUpdateCounts(String serviceName, TaskStatus status) {
        return taskUpdateRepository.countTaskUpdateByServiceNameEqualsAndStatusEquals(
                serviceName,
                status
        );
    }

    public List<TaskUpdate> getAllUpdates(Long jobId) {
        return taskUpdateRepository.findByJobId(jobId);
    }

    public List<Long> findDistinctJobIds() {
        return taskUpdateRepository.findDistinctJobId();
    }

    public void deleteTaskUpdatesForJob(Long jobId) {
        taskUpdateRepository.deleteByJobId(jobId);
    }
}
