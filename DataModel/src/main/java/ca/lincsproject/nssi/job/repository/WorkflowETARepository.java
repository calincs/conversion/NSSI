package ca.lincsproject.nssi.job.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkflowETARepository extends JpaRepository<WorkflowETA, Long> {
    List<WorkflowETA> findByWorkflowAndTierName(String workflow, String tierName);
}